<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchCity extends Model 
{
	use SoftDeletes;

    protected $fillable = ['city', 'search_id'];
    protected $dates = ['deleted_at'];

    public function search()
    {
        return $this->belongsTo('App\Search');
    }

}
