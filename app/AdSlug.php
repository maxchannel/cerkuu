<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdSlug extends Model
{
	use SoftDeletes;

    protected $table = 'ad_slugs';
	protected $fillable = ['content'];
	protected $dates = ['deleted_at'];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
}
