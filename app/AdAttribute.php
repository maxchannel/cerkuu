<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdAttribute extends Model
{
	use SoftDeletes;

	protected $table = 'ad_attributes';
	protected $fillable = ['name', 'content'];
	protected $dates = ['deleted_at'];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
}
