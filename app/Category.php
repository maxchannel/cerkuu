<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';

	public function ads()
    {
        return $this->hasMany('App\Ad','category_id','id');
    }

    public function ifCategoryExists($name)
	{
		if(Category::where('name',$name)->exists())
		{
			return true;
		}
	}

}
