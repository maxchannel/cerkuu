<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends Model 
{
	use SoftDeletes;

    protected $fillable = ['query'];
    protected $dates = ['deleted_at'];

    public function city()
    {
        return $this->hasOne('App\SearchCity');
    }

}
