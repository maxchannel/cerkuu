<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCancelation extends Model
{
	use SoftDeletes;
    protected $table = 'user_cancelations';
    protected $fillable = ['ad_id', 'user_id','comments'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
