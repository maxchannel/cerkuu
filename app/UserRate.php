<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    protected $table = 'user_rates';
    protected $fillable = ['rate','comments','user_id','from_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function from()
    {
        return $this->belongsTo('App\User','from_id','id');
    }

}
