<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JobSlug extends Model
{
    protected $table = 'job_slugs';
	protected $fillable = ['content'];

    public function job()
    {
        return $this->belongsTo('App\Job');
    }
}
