<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

	protected $table = 'ads';
    protected $fillable = ['title', 'description', 'city_name'];
    protected $dates = ['deleted_at'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Deleting related rows
    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($ad) 
        {
            $ad->key()->delete();
            $ad->images()->delete();
            $ad->attributes()->delete();
            $ad->reports()->delete();
            $ad->delivery()->delete();
            $ad->email()->delete();
            $ad->name()->delete();
            $ad->link()->delete();
            $ad->slug()->delete();
            $ad->telephone()->delete();
            //De users
            $ad->notifications()->delete();
        });
    }

    public function category()
    {
        return $this->hasOne('App\Category','id','category_id');
    }

    public function slug()
    {
        return $this->hasOne('App\AdSlug');
    }

    public function link()
    {
        return $this->hasOne('App\AdLink');
    }

    public function images()
    {
        return $this->hasMany('App\AdImage');
    }

    public function reports()
    {
        return $this->hasMany('App\AdReport');
    }

    public function notifications()
    {
        return $this->hasMany('App\UserNotification');
    }

    public function attributes()
    {
        return $this->hasMany('App\AdAttribute');
    }

    public function telephone()
    {
        return $this->hasOne('App\AdTelephone');
    }

    public function email()
    {
        return $this->hasOne('App\AdEmail');
    }

    public function name()
    {
        return $this->hasOne('App\AdName');
    }

    public function key()
    {
        return $this->hasOne('App\AdKey');
    }

    public function delivery()
    {
        return $this->hasOne('App\AdDelivery');
    }

    public function haveDelivery()
    {
        if($this->delivery()->exists())
        {
            return false;
        }else
        {
            return true;
        }
    }

    /*public function scopeTitle($query,$title)
    {
        if(trim($title) != "")
        {
            return $query->where('title','LIKE', '%'.$title.'%');
        }
    }*/

}

