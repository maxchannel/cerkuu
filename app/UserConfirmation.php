<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConfirmation extends Model
{
	protected $table = 'user_confirmations';
    protected $fillable = ['key','user_id'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
