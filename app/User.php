<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use SoftDeletes, Authenticatable, CanResetPassword;

    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'username'];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];

    public function ads()
    {
        return $this->hasMany('App\Ad');
    }

    //Deleting related rows
    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($user) 
        {
            $user->avatar()->delete();
            $user->notifications()->delete();
            $user->reports()->delete();
            $user->telephone()->delete();
            //Borrando las relaciones de los anuncios tambien
            foreach($user->ads as $ad)
            {
                $ad->attributes()->delete();
                $ad->delivery()->delete();
                $ad->email()->delete();
                $ad->images()->delete();
                $ad->link()->delete();
                $ad->slug()->delete();
                $ad->telephone()->delete();
                $ad->delete();
            }
        });
    }

    public function sales()
    {
        return $this->hasMany('App\UserSale','seller_id','id');
    }

    public function reports()
    {
        return $this->hasMany('App\UserReport');
    }

    //Rating y promedio del mismo
    public function rates()
    {
        return $this->hasMany('App\UserRate','user_id','id');
    }

    public function cancelations()
    {
        return $this->hasMany('App\UserCancelation','from_id','id');
    }

    public function notifications()
    {
        return $this->hasMany('App\UserNotification','user_id','id');
    }

    public function getNotifications()
    {
        return $this->notifications()->where('view',0)->count();
    }

    public function isConfirmed()
    {
        if($this->confirmation()->exists())
        {
            return false;
        }else
        {
            return true;
        }
    }

    //Valida si el ad es creado por el usuario
    public function isMyAd($ad_id)
    {
        if(Ad::where('id',$ad_id)->where('user_id',$this->id)->exists())
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function isMyJob($job_id)
    {
        if(Job::where('id',$job_id)->where('user_id',$this->id)->exists())
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function isMySale($sale_id)
    {
        if(UserSale::where('id',$sale_id)->where('seller_id',$this->id)->exists())
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function isMyBuy($sale_id)
    {
        if(UserSale::where('id',$sale_id)->where('buyer_id',$this->id)->exists())
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function avgRating()
    {
        return $this->rating()->selectRaw('avg(rate) as aggregate, user_id')->groupBy('user_id');
    }

    //Rating y promedio del mismo
    public function purchases()
    {
        return $this->hasMany('App\UserSale','buyer_id','id');
    }

    public function avatar()
    {
        return $this->hasOne('App\UserAvatar');
    }

    public function telephone()
    {
        return $this->hasOne('App\UserTelephone');
    }

    public function confirmation()
    {
        return $this->hasOne('App\UserConfirmation');
    }

    public function setPasswordAttribute($value)
    {
        if(!empty($value)) 
        {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function is($type)
    {
        return $this->type === $type;
    }

    public function isAdmin()
    {
        return $this->type === 'admin';
    }

    public function isStaff()
    {
        return $this->type === 'staff';
    }
}
