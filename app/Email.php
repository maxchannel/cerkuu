<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model 
{
	use SoftDeletes;

	protected $fillable = ['message', 'from', 'destiny'];
    protected $dates = ['deleted_at'];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
    
}
