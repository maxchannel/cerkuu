<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTelephone extends Model
{
	use SoftDeletes;
	protected $table = 'user_telephones';
	protected $fillable = ['user_id','number','whatsapp'];
	protected $dates = ['deleted_at'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
