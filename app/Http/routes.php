<?php

Route::get('/', ['as' => 'home', 'uses' => 'PublicController@index']);
Route::get('/privacy', ['as' => 'privacy', 'uses' => 'PublicController@privacy']);
Route::get('/terms', ['as' => 'terms', 'uses' => 'PublicController@terms']);

//Profile 
Route::get('/user/{id}', ['as' => 'user', 'uses' => 'UserController@showUser']);
//Buscar
Route::get('/search/advance', ['as' => 'searchAdvance', 'uses' => 'SearchController@index']);
Route::get('/search/advanced', ['as' => 'searchAdvanced', 'uses' => 'SearchController@search']);
//Anuncios
Route::get('/anuncios', ['as' => 'anuncios', 'uses' => 'AdController@anuncios']);
Route::get('/anuncio/{category}/{id}/{slug}', ['as' => 'anuncio', 'uses' => 'AdController@showAd']);//Anuncio
Route::get('/anuncios/category/{name}', ['as' => 'category', 'uses' => 'AdController@category']);//Categoria
Route::get('/anuncios/state/{id}', ['as' => 'state', 'uses' => 'SearchController@state']);//Categoria
//Print job
Route::get('/jobs/list', ['as' => 'jobList', 'uses' => 'JobController@jobList']);//Categoria
Route::get('/job/{id}/{slug}', ['as' => 'job', 'uses' => 'JobController@showJob']);//Anuncio
//Report
Route::get('/report/ad/{id}', ['as' => 'reportAd', 'uses' => 'PublicController@reportAd']);
Route::post('/report/ad/{id}', ['as' => 'reportAdStore', 'uses' => 'PublicController@reportAdStore']);
Route::get('/report/user/{id}', ['as' => 'reportUser', 'uses' => 'PublicController@reportUser']);
Route::post('/report/user/{id}', ['as' => 'reportUserStore', 'uses' => 'PublicController@reportUserStore']);
//Contact email
Route::get('/anuncio/email/{id}', ['as' => 'anuncioEmailing', 'uses' => 'EmailController@index']);
Route::post('/anuncio/email/{id}', ['as' => 'anuncioEmailingStore', 'uses' => 'EmailController@store']);
//ANONYMOUS
Route::get('/newAd', ['as' => 'newAnon', 'uses' => 'AnonController@newAd']);
Route::post('/newAd', ['as' => 'newAnonStore', 'uses' => 'AnonController@newAdStore']);  
//Adding <--
Route::get('/anuncio/edit/ses/pre/{id}', ['as' => 'preEditAnonAd', 'uses' => 'AnonController@preEditAnonAd']);
Route::post('/anuncio/edit/ses/pre/{id}', ['as' => 'preEditAnonAdSend', 'uses' => 'AnonController@preEditAnonAdSend']);
Route::get('/anuncio/edit/ses/{id}/{key}', ['as' => 'editAnonAd', 'uses' => 'AnonController@editAnonAd']);
Route::put('/anuncio/edit/ses/{id}/{key}', ['as' => 'editAnonAdStore', 'uses' => 'AnonController@editAnonAdStore']);
Route::get('/anuncio/edit-image/ses/{id}/{key}', ['as' => 'editAnonImageAd', 'uses' => 'AnonController@editAnonImageAd']);
Route::put('/anuncio/edit-image/ses/{id}/{key}', ['as' => 'editAnonImageAdStore', 'uses' => 'AnonController@editAnonImageAdStore']);
Route::delete('/anuncio/delete-ad/ses/{id}/{key}', ['as' => 'deleteAdAnon', 'uses' => 'AnonController@destroyAd']);
Route::delete('/anuncio/delete-image/ses/{id}/{key}/{image}', ['as' => 'deleteAdAnonImage', 'uses' => 'AnonController@destroy']);
//ANONYMOUS

//Guest users
Route::group(['middleware' => 'guest'], function () {
    //Login
    Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@login']);
    Route::post('/login', ['as' => 'loginSend', 'uses' => 'AuthController@loginSend']);
    //Singup
    Route::get('/signup', ['as' => 'signup', 'uses' => 'UserController@signup']);
    Route::post('/signup', ['as' => 'signupStore', 'uses' => 'UserController@signupStore']);      
});

//Auth users
Route::group(['middleware' => 'auth'], function () {

    //Solo si el user creo el anuncio
    Route::group(['middleware' => 'my_ad'], function () {
        //Editar Anuncio
        Route::get('/editad/{id}', ['as' => 'editAd', 'uses' => 'AdController@editAd']);
        Route::put('/editad/{id}', ['as' => 'editAdUpdate', 'uses' => 'AdController@editAdUpdate']);
        //Ediar Republish Ad
        Route::put('/edit-ad/{id}', ['as' => 'editAdRepublishUpdate', 'uses' => 'RepublishController@update']);
        //Editar
        Route::get('/editad/image/{id}', ['as' => 'editAdImage', 'uses' => 'AdController@editAdImage']);
        Route::put('/editad/image/{id}', ['as' => 'editAdImageUpdate', 'uses' => 'AdController@editAdImageUpdate']);
        //Editar jobs
        Route::get('/editjob/{id}', ['as' => 'editJob', 'uses' => 'JobController@editJob']);
        Route::put('/editjob/{id}', ['as' => 'editJobUpdate', 'uses' => 'JobController@editJobUpdate']);
        //Eliminar Anuncio Indie
        Route::delete('/deleteadi/{id}', ['as' => 'deleteAdIndie', 'uses' => 'AdminController@deleteAdIndie']); 
    });
    
    //Solo si el user creo la vacante
    Route::group(['middleware' => 'my_job'], function () {
        //Editar jobs
        Route::get('/editjob/{id}', ['as' => 'editJob', 'uses' => 'JobController@editJob']);
        Route::put('/editjob/{id}', ['as' => 'editJobUpdate', 'uses' => 'JobController@editJobUpdate']);
    });

    Route::delete('/editad/image/destroy/{id}', ['as' => 'editAdImageDestroy', 'uses' => 'AdController@destroy']);

    //Solo si es mi venta
    Route::group(['middleware' => 'my_sale'], function () {
        Route::get('/buy/contact-buyer/{sale_id}', ['as' => 'buyContactBuyer', 'uses' => 'BuyController@buyContactBuyer']);
        Route::post('/buy/contact-buyer/{sale_id}', ['as' => 'buyContactBuyerSend', 'uses' => 'BuyController@buyContactBuyerSend']);
        Route::get('/buy/rating-buyer/{sale_id}', ['as' => 'buyRatingBuyer', 'uses' => 'BuyController@buyRatingBuyer']);
        Route::post('/buy/rating-buyer/{sale_id}', ['as' => 'buyRatingBuyerSend', 'uses' => 'BuyController@buyRatingBuyerSend']);
    });

    //Solo si es mi compra
    Route::group(['middleware' => 'my_buy'], function () {
        Route::get('/buy/contact/{sale_id}', ['as' => 'buyContact', 'uses' => 'BuyController@buyContactSeller']);
        Route::post('/buy/contact/{sale_id}', ['as' => 'buyContactSend', 'uses' => 'BuyController@buyContactSellerSend']);
        Route::get('/buy/rating-seller/{sale_id}', ['as' => 'buyRatingSeller', 'uses' => 'BuyController@buyRatingSeller']);
        Route::post('/buy/rating-seller/{sale_id}', ['as' => 'buyRatingSellerSend', 'uses' => 'BuyController@buyRatingSellerSend']);
    });

    Route::get('/notifications', ['as' => 'notifications', 'uses' => 'UserController@notifications']);
    //Messages
    //Route::get('/messages', ['as' => 'messages', 'uses' => 'MessengerController@index']);
    //Settings
    Route::get('/settings/profile', ['as' => 'settings_profile', 'uses' => 'UserController@settings']);
    Route::put('/settings/profile', ['as' => 'settings_profile_update', 'uses' => 'UserController@settingsUpdate']); 
    Route::get('/settings/avatar', ['as' => 'settings_avatar', 'uses' => 'UserController@settingsAvatar']);
    Route::put('/settings/avatar', ['as' => 'settings_avatar_update', 'uses' => 'UserController@settingsAvatarUpdate']);
    Route::get('/settings/email', ['as' => 'settings_email', 'uses' => 'UserController@settingsEmail']);
    Route::put('/settings/email', ['as' => 'settings_email_update', 'uses' => 'UserController@settingsEmailUpdate']);
    Route::get('/settings/forwardcode', ['as' => 'forwardcode', 'uses' => 'UserController@forwardcode']);
    Route::put('/settings/forwardcode', ['as' => 'forwardcode_send', 'uses' => 'UserController@forwardcode_send']);
    //Settings Confirm Email
    Route::get('/settings/confirmemail', ['as' => 'confirm_email', 'uses' => 'UserController@confirm_email']);
    Route::post('/settings/confirmemail', ['as' => 'confirm_email_update', 'uses' => 'UserController@confirm_email_update']);
    //Nuevo anuncio
    Route::get('/new-ad', ['as' => 'new', 'uses' => 'AdController@newAd']);
    Route::post('/new-ad', ['as' => 'newAdStore', 'uses' => 'AdController@newAdStore']);
    Route::get('/myads', ['as' => 'myAds', 'uses' => 'AdController@myAds']);
    Route::get('/mysales', ['as' => 'mySales', 'uses' => 'AdController@mySales']);
    Route::get('/mypurchases', ['as' => 'myPurchases', 'uses' => 'AdController@myPurchases']);
    Route::get('/myvacancies', ['as' => 'myVacancies', 'uses' => 'JobController@myVacancies']);
    //Comprar articulo
    Route::get('/buy/{ad_id}', ['as' => 'buy', 'uses' => 'BuyController@buy']);
    Route::post('/buy/{ad_id}', ['as' => 'buyConfirm', 'uses' => 'BuyController@buyConfirm']);
    //Logout
    Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
});

//Admin users
Route::group(['prefix' => 'admin', 'middleware' => ['auth','is_admin']], function () {
    Route::get('/', ['as' => 'adminNewUsers', 'uses' => 'AdminController@newUsers']);
    Route::get('/approve', ['as' => 'adminNewAds', 'uses' => 'AdminController@newAds']);
    Route::get('/expired', ['as' => 'adminExpiredAds', 'uses' => 'AdminController@expiredAds']);
    Route::get('/rates', ['as' => 'adminRates', 'uses' => 'AdminController@newRates']);
    Route::get('/emails', ['as' => 'adminNewEmails', 'uses' => 'AdminController@newEmails']);
    Route::get('/statics', ['as' => 'adminStatics', 'uses' => 'AdminController@statics']);
    //Reported
    Route::get('/reported/users', ['as' => 'reportedUsers', 'uses' => 'AdminController@reportedUsers']);
    Route::post('/unreport/users/{id}', ['as' => 'unreportUser', 'uses' => 'AdminController@unreportUser']);
    Route::get('/reported/ads', ['as' => 'reportedAds', 'uses' => 'AdminController@reportedAds']);
    Route::post('/unreport/ad/{id}', ['as' => 'unreportAd', 'uses' => 'AdminController@unreportAd']); 
    
    //Publicar trabajo
    Route::get('/job', ['as' => 'postJob', 'uses' => 'JobController@job']);
    Route::post('/job', ['as' => 'jobStore', 'uses' => 'JobController@jobStore']);

    //Eliminar Anuncio
    Route::delete('/deletead/{id}', ['as' => 'deleteAd', 'uses' => 'AdminController@deleteAd']); 
    //Eliminar Anuncio Expirado
    Route::delete('/deleteExpired/{id}', ['as' => 'deleteExpired', 'uses' => 'AdminController@deleteExpired']);
    //Eliminar Email
    Route::delete('/deleteEmail/{id}', ['as' => 'deleteEmail', 'uses' => 'AdminController@deleteEmail']);  
    //Eliminar usuario
    Route::delete('/deleteuser/{id}', ['as' => 'deleteUser', 'uses' => 'AdminController@deleteUser']); 
    
    //Activar Anuncio
    Route::post('/activatead/{id}', ['as' => 'activateAd', 'uses' => 'AdminController@activateAd']); 
    Route::delete('/activateuser/{id}', ['as' => 'activateUser', 'uses' => 'AdminController@activateUser']); 
    //Activar Email
    Route::post('/activateemail/{id}', ['as' => 'activateEmail', 'uses' => 'AdminController@activateEmail']); 
    
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth','is_admin_staff']], function () {
    //Republicar anuncio
    Route::get('/republish', ['as' => 'republish', 'uses' => 'RepublishController@create']);
    Route::post('/republish', ['as' => 'republishAdStore', 'uses' => 'RepublishController@store']);
    //Publicar trabajo
    Route::get('/job', ['as' => 'postJob', 'uses' => 'JobController@job']);
    Route::post('/job', ['as' => 'jobStore', 'uses' => 'JobController@jobStore']);
    //Scraper
    Route::get('/scraper', ['as' => 'scraper', 'uses' => 'ScraperController@scraper']);
    Route::get('/scraping', ['as' => 'scraping', 'uses' => 'ScraperController@scraping']);
    Route::post('/scrapingStore', ['as' => 'scrapingStore', 'uses' => 'ScraperController@store']);
    Route::get('/scraperLogout', ['as' => 'scraperLogout', 'uses' => 'ScraperController@scraperLogout']);
    //Masivo
    Route::get('/masivo', ['as' => 'masivo', 'uses' => 'ScraperController@masivo']);
    Route::post('/masivo/store', ['as' => 'masivoStore', 'uses' => 'ScraperController@masivoStore']);
});

//API
Route::get('/api/cities', ['as' => 'ajax-city', 'uses' => 'AjaxController@city']);







