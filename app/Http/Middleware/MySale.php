<?php namespace App\Http\Middleware;

use Closure;

class MySale
{
    //Valida si es mi venta
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isMySale($request->sale_id))
        {
            return redirect('/');
        }

        return $next($request);
    }
}
