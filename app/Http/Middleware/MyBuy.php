<?php namespace App\Http\Middleware;

use Closure;

class MyBuy
{
    //Valida si es mi venta
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isMyBuy($request->sale_id))
        {
            return redirect('/');
        }

        return $next($request);
    }
}
