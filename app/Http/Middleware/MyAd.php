<?php namespace App\Http\Middleware;

use Closure;

class MyAd
{
    //Valida si el ad es creado por el usuario
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isMyAd($request->id)) 
        {
            return redirect('/');
        }

        return $next($request);
    }
}
