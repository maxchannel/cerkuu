<?php namespace App\Http\Middleware;

use Closure;

class MyJob
{
    //Valida si el ad es creado por el usuario
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isMyJob($request->id)) 
        {
            return redirect('/');
        }

        return $next($request);
    }
}
