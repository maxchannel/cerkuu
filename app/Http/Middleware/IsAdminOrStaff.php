<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Auth\Guard;

class IsAdminOrStaff extends IsType
{
    public function getType()
    {
        return 'staff';
    }

}

/*class IsAdminOrStaff implements Middleware 
{
    private $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        return $next($request);
        
        if(!$this->auth->user()->is('staff'))
        {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }else
            {
                return redirect()->to('/');
            }
        }

    }

}*/