<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',

		//Areas renstringidas
        'is_admin' => 'App\Http\Middleware\IsAdmin',
        'is_admin_staff' => 'App\Http\Middleware\IsAdminOrStaff',
        //Ads
        'my_ad' => 'App\Http\Middleware\MyAd',
        'my_job' => 'App\Http\Middleware\MyJob',
        'my_sale' => 'App\Http\Middleware\MySale',
        'my_buy' => 'App\Http\Middleware\MyBuy',
	];

}
