<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Ad;

class StoreNewSaleRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ad_id' => 'required|integer|exists:ads,id',
            'seller_id' => 'required|integer|exists:users,id'
        ];
    }
}
