<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmailContactRequest extends Request 
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'title' => 'required|max:100',
			'email' => 'required|email|max:70',
			'message' => 'required|max:2000',
		];
	}

}
