<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewRatingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'rate'=>'required',
            'comments'=>'max:600',
        ];
    }
}
