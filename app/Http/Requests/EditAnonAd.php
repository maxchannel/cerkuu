<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditAnonAd extends Request 
{
	public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'title' => 'required|min:8|max:100',
            'description' => 'required|min:8|max:1100',
            'price' => 'required|integer',
            'city_name'=> 'required|min:3|max:70',
            'name' => 'required|min:3|max:100',
            'telephone' => 'numeric',
            'delivery'=> 'max:70',
        ];

        if($this->request->get('names') != "")
        {
            foreach($this->request->get('names') as $key => $val)
            {
                $rules['names.'.$key] = 'max:50';
            }
        }

        if($this->request->get('contents') != "")
        {
            foreach($this->request->get('contents') as $key => $val)
            {
                $rules['contents.'.$key] = 'max:50';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        if($this->request->get('names') != "")
        {
            foreach($this->request->get('names') as $key => $val)
            {
                $messages['names.'.$key.'.max'] = 'El campo "Nombre '.$key.'" no debe ser mayor que :max caracteres.';
            }
        }

        if($this->request->get('contents') != "")
        {
            foreach($this->request->get('contents') as $key => $val)
            {
                $messages['contents.'.$key.'.max'] = 'El campo "Contenido '.$key.'" no debe ser mayor que :max caracteres.';
            }
        }
        
        return $messages;
    }

}
