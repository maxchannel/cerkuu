<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ScrapeAdStoreRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];
		for($i=1; $i<=10; $i++) 
		{
            $rules['title'.$i] = 'required|min:3|max:100';
            $rules['description'.$i] = 'required|min:3|max:2000';
            $rules['city_name'.$i] = 'required|max:70';
            $rules['telephone'.$i] = 'numeric|required_without_all:link'.$i.',email'.$i.'';
            $rules['email'.$i] = 'email|required_without_all:telephone'.$i.',link'.$i.'';
            $rules['link'.$i] = 'url|required_without_all:telephone'.$i.',email'.$i.'';
            $rules['category_id'.$i] = 'required|integer';
            $rules['delivery'.$i] = 'max:70';
            $rules['price'.$i] = 'integer';
        }

        return $rules;
	}

}
