<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreReportUserRequest extends Request 
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'report'=>'required|integer',
			'name' => 'required|max:100',
			'email' => 'required|email|max:60',
			'telephone' => 'numeric',
            'comments'=>'max:2000',
		];
	}

}
