<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditUserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'username' => 'required|alpha_num|min:3|max:16|unique:users,username,'.\Auth::user()->id,
            'number' => 'numeric',
        ];
    }
}
