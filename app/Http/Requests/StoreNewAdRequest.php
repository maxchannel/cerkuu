<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewAdRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'title' => 'required|min:8|max:100',
            'description' => 'required|min:8|max:2000',
            'price' => 'required|integer',
            'city_name'=> 'required|max:70',
            'category_id' => 'required|integer',
            'delivery'=> 'max:70',
        ];

        foreach($this->request->get('names') as $key => $val)
        {
            $rules['names.'.$key] = 'max:50';
        }

        foreach($this->request->get('contents') as $key => $val)
        {
            $rules['contents.'.$key] = 'max:50';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('names') as $key => $val)
        {
            $messages['names.'.$key.'.max'] = 'El campo "Nombre '.$key.'" no debe ser mayor que :max caracteres.';
        }
        foreach($this->request->get('contents') as $key => $val)
        {
            $messages['contents.'.$key.'.max'] = 'El campo "Contenido '.$key.'" no debe ser mayor que :max caracteres.';
        }
        return $messages;
    }
}
