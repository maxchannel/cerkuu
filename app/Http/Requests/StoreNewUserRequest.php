<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewUserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'email' => 'required|email|unique:users|max:60',
            'username' => 'required|unique:users|alpha_num|min:3|max:16',
            'password' => 'required|min:6|max:25'
        ];
    }
}
