<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ScrapeAdRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];
		for($i=1; $i<=10; $i++) 
		{
            $rules['id'.$i] = 'required|numeric';
        }

        return $rules;
	}

}
