<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditAdRepublishRequest extends Request 
{
	public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'title' => 'required|min:6|max:100',
            'description' => 'required|min:6|max:2000',
            'city_name'=> 'required|max:70',
            'telephone' => 'numeric|required_without_all:link,email',
            'email' => 'email|required_without_all:telephone,link',
            'link' => 'url|required_without_all:telephone,email',
            //'category_id' => 'required|integer',
            'delivery'=> 'max:70',
            'price' => 'integer',
        ];

        if($this->request->get('names') != "")
        {
            foreach($this->request->get('names') as $key => $val)
            {
                $rules['names.'.$key] = 'max:50';
            }
        }

        if($this->request->get('contents') != "")
        {
            foreach($this->request->get('contents') as $key => $val)
            {
                $rules['contents.'.$key] = 'max:50';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        if($this->request->get('names') != "")
        {
            foreach($this->request->get('names') as $key => $val)
            {
                $messages['names.'.$key.'.max'] = 'El campo "Nombre '.$key.'" no debe ser mayor que :max caracteres.';
            }
        }

        if($this->request->get('contents') != "")
        {
            foreach($this->request->get('contents') as $key => $val)
            {
                $messages['contents.'.$key.'.max'] = 'El campo "Contenido '.$key.'" no debe ser mayor que :max caracteres.';
            }
        }
        
        return $messages;
    }

}
