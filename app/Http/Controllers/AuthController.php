<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login()
    {
        return view('public.login');
    }

    public function loginSend()
    {
        $data = \Request::only('email','password','remember');
        $rules = ['email' => 'required|email','password' => 'required'];
        $validation = \Validator::make($data, $rules);

        if(!$validation->fails())
        {
            $credentials =['email'=>$data['email'], 'password'=>$data['password']];

            if(\Auth::attempt($credentials,$data['remember']))
            {
                return \Redirect::route('home');
            }else
            {
                return \Redirect::back()->withInput()->with('login_error', 1);
            }

        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }

    }

    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('home');
    }

}
