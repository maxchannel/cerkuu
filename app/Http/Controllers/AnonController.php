<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserReport;
use App\Ad;
use App\AdEmail;
use App\AdName;
use App\AdTelephone;
use App\AdLink;
use App\AdSlug;
use App\AdKey;
use App\AdDelivery;
use App\AdAttribute;
use App\AdImage;
use App\Category;
use App\Http\Requests\StoreReportAdRequest;
use App\Http\Requests\StoreNewAdAnonRequest;
use App\Http\Requests\StoreReportUserRequest;
use App\Http\Requests\EditAnonAd;

class AnonController extends Controller 
{

    public function newAd()
    {
        return view('ads.add.newAnon');
    }

    public function newAdStore(StoreNewAdAnonRequest $request)
    {
        //return $request->input('city_id');

        $newAd = new Ad;
        if(\Auth::check())
        {
            $newAd->user_id = \Auth::user()->id;
            $newAd->role = 'anonuser';
        }else
        {
            $newAd->user_id = 1;
            $newAd->role = 'anon';
        }
        $newAd->active = 0;
        $newAd->type = 'ofert';
        $newAd->price = $request->input('price');
        $newAd->description = $request->input('description'); 
        $newAd->category_id = $request->input('category_id');
        $newAd->fill($request->all());
        $newAd->save();

        //Slug
        $slug = new AdSlug;
        $slug->content = str_slug($newAd->title);
        $slug->ad_id = $newAd->id;
        $slug->save();

        //Key
        if(!\Auth::check())
        {
            $key = new AdKey;
            $key->content = str_random(7);
            $key->ad_id = $newAd->id;
            $key->save();
        }

        //Email
        $email = new AdEmail();
        $email->ad_id = $newAd->id;
        $email->content = $request->input('email');
        $email->save();

        //Name
        $email = new AdName();
        $email->ad_id = $newAd->id;
        $email->content = $request->input('name');
        $email->save();

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = new AdAttribute();
                $attribute->ad_id = $newAd->id;
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 12)//Solo 12 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(6).".".$extension;    
                        ///Move file to images/post
                        $file->move('images/ads/',$newName); 
                        //Insert in db
                        $image = new AdImage();
                        $image->ad_id = $newAd->id;
                        $image->route = $newName;
                        $image->save();    
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 12 imágenes al anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image

        //Email
        if($request->input('email'))
        {
            $subject = 'Anuncio Creado: '.$newAd->title;
            $to = $request->input('email');
            if(\Auth::check())
            {
                $content = 'Creaste un anuncio en FijaAnuncios el anuncio tendrá un ciclo de vida de 120 días.';
            }else
            {
                $content = 'Creaste un anuncio en FijaAnuncios el anuncio tendrá un ciclo de vida de 120 días. Contraseña de tu anuncio: '.$key->content;
            }
            $params = [
                'subject'=>$subject,
                'content'=>$content,
                'route'=> route('anuncio', [$newAd->category->name, $newAd->id, $newAd->slug->content]),
            ];

            \Mail::send('emails.transactional.action', $params, function($message) use ($subject,$to)
            {
                $message->to($to)->subject($subject);
                $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');    

            });
        }
        //Email

        //Entregas
        if($request->input('delivery'))
        {
            $delivery = new AdDelivery();
            $delivery->ad_id = $newAd->id;
            $delivery->place = $request->input('delivery');
            $delivery->save();
        }

        //Telephone relacionado
        if($request->input('telephone'))
        {
            $delivery = new AdTelephone();
            $delivery->ad_id = $newAd->id;
            $delivery->content = $request->input('telephone');
            $delivery->save();
        }

        //Category
        $category = Category::find($request->input('category_id'));

        return \Redirect::back()->with(['id'=>$newAd->id,'category'=>$category->name,'slug'=>$slug->content]);
    }

	public function preEditAnonAd($id)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon')
        {
            return view('ads.edit.preEdit', compact('ad'));
        }else
        {
            return \Redirect::route('home');
        }
    }

    public function preEditAnonAdSend($id)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon')
        {
            $rules = ['password' => 'required'];
            $validation = \Validator::make(\Input::all(), $rules);
            if(!$validation->fails())
            {
                if($ad->key->content === \Request::input('password'))//key is correct
                {
                    return \Redirect::route('editAnonAd', [$ad->id, \Request::input('password')]);
                }else
                {
                    return \Redirect::back()->with('message', 'Password de anuncio incorrecto');
                }
            }else 
            {
                return \Redirect::back()->withErrors($validation);
            }
        }else
        {
            return \Redirect::route('home');
        }
    }

    public function editAnonAd($id, $key)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content === $key)
        {
            return view('ads.edit.anon', compact('ad', 'key'));
        }else
        {
            return \Redirect::route('home');
        }
    }

    public function editAnonAdStore($id, $key, EditAnonAd $request)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content == $key)
        {
            $ad->active = 0;
            $ad->title = $request->input('title');
            $ad->description = $request->input('description');
            $ad->price = $request->input('price');
            $ad->city_name = $request->input('city_name');    

            //Atributos
            $names = $request->input('names');
            $contents = $request->input('contents');
            $attrs = $request->input('atrs_id');
            for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
            { 
                if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
                {  
                    $attribute = AdAttribute::find($attrs[$i]);
                    $attribute->name = $names[$i];
                    $attribute->content = $contents[$i];
                    $attribute->save();
                }
            }    

            //Email relacionado
            /*if($request->input('email'))
            {
                AdEmail::where('ad_id', $ad->id)->update(['content' => $request->input('email')]);
            } */   

            //Telephone relacionado
            if($request->input('telephone'))
            {
                AdTelephone::where('ad_id', $ad->id)->update(['content' => $request->input('telephone')]);
            }

            //Telephone relacionado
            if($request->input('name'))
            {
                AdName::where('ad_id', $ad->id)->update(['content' => $request->input('name')]);
            }

            //Delivery
            if($request->input('delivery') != "")
            {
                if($ad->delivery()->exists())
                {
                    AdDelivery::where('ad_id', $ad->id)->update(['place' => $request->input('delivery')]);
                }else
                {
                    $delivery = new AdDelivery;
                    $delivery->ad_id = $ad->id;
                    $delivery->place = $request->input('delivery');
                    $delivery->save();
                }
            }    

            $ad->save();    

            return \Redirect::back()->with('message', 'Anuncio Actualizado');
        }else
        {
            return \Redirect::route('home');
        }

    }

    public function editAnonImageAd($id, $key)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content === $key)
        {
            $images = AdImage::where('ad_id',$ad->id)->orderBy('created_at','DESC')->get();
            return view('ads.edit.anonImages', compact('ad', 'key', 'images'));
        }else
        {
            return \Redirect::route('home');
        }
    }

    public function editAnonImageAdStore($id, $key)
    {
        $ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content == $key)
        {
            $ad->active = 0;
            //Image
            $files = \Input::file('file');
            if(!empty($files[0]))//Solo si se manda 1 imagen
            {
                foreach($files as $file) 
                {
                    // Validate files from input file
                    $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                    $validation = \Validator::make(['file'=> $file], $rules);        

                    if(!$validation->fails()) 
                    {
                        //Limitando el numero de imagenes
                        if($ad->images()->count() < 12)
                        {
                            //Rename file
                            $extension = $file->getClientOriginalExtension(); 
                            $newName = str_random(6).".".$extension;            

                            //Insert in db
                            $image = new AdImage();
                            $image->ad_id = $ad->id;
                            $image->route = $newName;
                            $image->save();            

                            ///Move file to images/post
                            $file->move('images/ads/',$newName); 
                        }else
                        {
                            return \Redirect::back()->with('image-message', 'Alcansaste el limite de 12 imágenes por anuncio');
                        }    

                    }else 
                    {
                        \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes menores a 4MB');
                    }
                }
            }else
            {
                return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
            }    

            $ad->save();
            return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu anuncio']);
        }else
        {
            return \Redirect::route('home');
        }

    }

    public function destroy($id, $key, $image)
    {
    	$ad = Ad::find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content == $key)
        {
        	AdImage::find($image)->delete();
            return \Redirect::back()->with('message', 'Imágen eliminada con exito');
        }else
        {
            return \Redirect::route('home');
        }
    }

    public function destroyAd($id, $key)
    {
    	$ad = Ad::find($id);
        $this->notFoundUnless($ad);

        //Si es anonimo
        if($ad->role == 'anon' && $ad->key->content == $key)
        {
            //Email
            $subject = 'Anuncio Eliminado: '.$ad->title;
            $to = $ad->email->content;
            $content = 'Eliminaste exitosamente tu anuncio en FijaAnuncios';
            $params = [
                'subject'=>$subject,
                'content'=>$content,
            ];    

            \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
            {
                $message->to($to)->subject($subject);
                $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');    

            });
            //Email    

            $ad->delete();//Eliminando
            \Session::flash('message', 'Anuncio Eliminado');
            return \Redirect::route('home');
        }else
        {
            return \Redirect::route('home');
        }
    }

}