<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewJobRequest;
use App\Job;
use App\JobSlug;
use App\JobAttribute;
use App\User;

class JobController extends Controller
{
    public function showJob($id,$slug)
    {
        $job = Job::with('user')->with('slug')->find($id);
        $this->notFoundUnless($job);
        
        if($slug != $job->slug->content)//redirigir a slug correcto
        {
            //Redirige tambien a la categoria correcta
            return \Redirect::route('job', [$job->id, $job->slug->content]);
        }

        return view('jobs.job', compact('job'));
    }

    public function myVacancies()
    {
        $jobs = Job::where('user_id',\Auth::user()->id)->orderBy('created_at','DESC')->paginate(20);

        return view('jobs.myVacancies', compact('jobs'));
    }

    public function job()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('jobs.post-job', compact('user'));
    }

    public function jobStore(StoreNewJobRequest $request)
    {
        //return $request->input();
        $job = new Job;
        $job->user_id = \Auth::user()->id;
        $job->active = 0;
        $job->fill($request->all());
        $job->save();

        //Slug
        $slug = new JobSlug;
        $slug->content = str_slug($job->title);
        $slug->job_id = $job->id;
        $slug->save();

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = new JobAttribute();
                $attribute->job_id = $job->id;
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        /*$

        //Email
        $subject = 'Anuncio Creado: '.$job->title;
        $to = \Auth::user()->email;
        $content = 'Creaste un anuncio en FijaAnuncios el anuncio tendrá un ciclo de vida de 120 días.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
            'route'=> route('anuncio', [$job->category->name, $job->id, $job->slug->content]),
        ];

        \Mail::send('emails.transactional.action', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        //Salary
        if($request->input('delivery'))
        {
            $delivery = new Delivery();
            $delivery->ad_id = $job->id;
            $delivery->place = $request->input('delivery');
            $delivery->save();
        }

        */

        return \Redirect::back()->with(['message'=>'Vacante creada']);
    }

    public function editJob($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.editJob', compact('job'));
    }

    public function editJobUpdate($id, StoreNewJobRequest $request)
    {
        $job = Job::findOrFail($id);
        $job->active = 0;
        $job->fill($request->all());

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        $attrs = $request->input('atrs_id');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = AttributeJob::find($attrs[$i]);
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Salary
        /*if($request->input('delivery') != "")
        {
            if($job->delivery()->exists())
            {
                Delivery::where('ad_id', $job->id)->update(['place' => $request->input('delivery')]);
            }else
            {
                $delivery = new Delivery;
                $delivery->ad_id = $job->id;
                $delivery->place = $request->input('delivery');
                $delivery->save();
            }
        }*/

        $job->save();

        return \Redirect::back()->with('message', 'Anuncio Actualizado');
    }

    public function jobList()
    {
        $jobs = Job::orderBy('created_at','DESC')->paginate(20);

        return view('jobs.list', compact('jobs'));
    }


}
