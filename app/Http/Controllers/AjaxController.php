<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\City;

class AjaxController extends Controller
{

    public function city()
    {
        $term = \Input::get('term');
        $results = array();
        $queries = City::where('name','LIKE', '%'.$term.'%')->take(8)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => ucfirst($query->name)];
        }    

        return \Response::json($results);
    }

}
