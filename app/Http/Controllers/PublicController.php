<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\User;
use App\UserReport;
use App\Ad;
use App\AdEmail;
use App\AdTelephone;
use App\AdLink;
use App\AdSlug;
use App\AdKey;
use App\AdDelivery;
use App\AdAttribute;
use App\AdImage;
use App\AdReport;
use App\AdName;
use App\Category;
use App\Http\Requests\StoreReportAdRequest;
use App\Http\Requests\StoreNewAdAnonRequest;
use App\Http\Requests\StoreReportUserRequest;
use App\Http\Requests\EditAnonAd;


class PublicController extends Controller
{

    public function index()
    {
        $consulta = Ad::with('images')->orderBy('created_at','DESC');
        $sort = \Request::input('sort');

        if($sort)
        {
            if($sort == 'price')
            {
                $consulta = Ad::orderBy('price', 'DESC');
            }elseif($sort == 'price_a')
            {
                $consulta = Ad::orderBy('price', 'ASC');
            }elseif($sort == 'date')
            {
                $consulta = Ad::orderBy('created_at', 'DESC');
            }elseif($sort == 'date_a')
            {
                $consulta = Ad::orderBy('created_at', 'ASC');
            }
        }


        //$ads = Ad::with('images')->orderBy('created_at','DESC')->paginate(33);
        $ads = $consulta->paginate(35);
        return view('home', compact('ads'));
    }

    public function privacy()
    {
        return view('public.privacy');

    }

    public function terms()
    {
        return view('public.terms');

    }

    public function reportAd($id)
    {
    	$ad = Ad::with('images')->find($id);
        $this->notFoundUnless($ad);

        return view('admin.reportAd', compact('ad'));
    }

    public function reportAdStore($id, StoreReportAdRequest $request)
    {
    	$reportAd = new AdReport;
    	$reportAd->report = $request->input('report');
        $reportAd->name = $request->input('name');
        $reportAd->telephone = $request->input('telephone');
    	$reportAd->comments = $request->input('comments');
    	$reportAd->email = $request->input('email');
    	$reportAd->ad_id = $id;
    	$reportAd->save();
    	
    	return \Redirect::back()->with('message', 'Reporte Enviado');
    }

    public function reportUser($id)
    {
    	$user = User::find($id);
        $this->notFoundUnless($user);

        return view('admin.reportUser', compact('user'));
    }

    public function reportUserStore($id, StoreReportUserRequest $request)
    {
    	$reportUser = new UserReport;
    	$reportUser->report = $request->input('report');
        $reportUser->name = $request->input('name');
        $reportUser->telephone = $request->input('telephone');
    	$reportUser->comments = $request->input('comments');
    	$reportUser->email = $request->input('email');
    	$reportUser->user_id = $id;
    	$reportUser->save();
    	
    	return \Redirect::back()->with('message', 'Reporte Enviado');
    }

}