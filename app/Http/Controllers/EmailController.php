<?php namespace App\Http\Controllers;

use App\Ad;
use App\Email;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EmailContactRequest;

class EmailController extends Controller 
{
	public function index($id)
	{
		$ad = Ad::with('user')->find($id);
        $this->notFoundUnless($ad);

		return view('users.contact.email', compact('ad'));
	}

	public function store($id, EmailContactRequest $request)
	{
		$ad = Ad::with('user')->find($id);
        $this->notFoundUnless($ad);

	    //Email db
        $email = new Email;
        $email->message = $request->input('message');
        $email->from = $request->input('email');
        $email->destiny = $ad->email->content;
        $email->send = 0;
        $email->ad_id = $id;
        $email->save();

        return \Redirect::back()->with('message', '1');
	}

}
