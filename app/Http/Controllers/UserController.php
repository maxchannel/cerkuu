<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserAvatar;
use App\UserSale;
use App\UserIp;
use App\UserTelephone;
use App\UserConfirmation;
use App\UserCancelation;
use App\UserNotification;
use App\Http\Requests\StoreNewUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\EditUserEmailRequest;

class UserController extends Controller
{
    public function showUser($id)
    {
        //Authors::where($column , '=', $id)->first();
        $user = User::find($id);
        $this->notFoundUnless($user);
        $sales = UserSale::onlyTrashed()->where('seller_id',$id)->where('seller_cancel',0)->get();
        $purchases = UserSale::onlyTrashed()->where('buyer_id',$id)->where('buyer_cancel',0)->get();
        $cancelations = UserCancelation::where('user_id',$id)->count();

        return view('users.profile', compact('user','cancelations','sales','purchases'));
    }

    public function notifications()
    {
        //Cambiando a visto
        if(\Auth::user()->getNotifications() > 0)
        {
            UserNotification::where('user_id', \Auth::user()->id)->update(array('view' => 1));
        }
        //Enviando para tabular
        $notifications = UserNotification::where('user_id', \Auth::user()->id)->orderBy('created_at','DESC')->paginate(20);

        return view('users.notifications', compact('notifications'));
    }

    public function signup()
    {
        return view('users.signup');
    }

    public function signupStore(StoreNewUserRequest $request)
    {
        $user = new User($request->all());
        $user->type = 'user';
        $user->save();

        $confirmation = new UserConfirmation();
        $confirmation->user_id = $user->id;
        $confirmation->key = str_random(5);
        $confirmation->save();

        $avatar = new UserAvatar();
        $avatar->user_id = $user->id;
        $avatar->route = 'default.jpg';
        $avatar->save();

        //Bienvenida
        $notification = new UserNotification();
        $notification->type = 'welcome';
        $notification->view = 0;
        $notification->ad_id = 1;
        $notification->user_id = $user->id;
        $notification->save();

        //Telephone
        $tel = new UserTelephone();
        $tel->user_id = $user->id;
        $tel->number = '';
        $tel->whatsapp = 0;
        $tel->save();

        $ip = new UserIp();
        $ip->user_id = $user->id;
        $ip->content = \Request::getClientIp();
        $ip->save();

        //Email
        $subject = 'Código de confirmación';
        $to = $user->email;
        $content = 'Tu código de confirmación es: '.$confirmation->key;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        return \Redirect::route('login')->with('message', 'Registro exitoso, ahora puedes iniciar sesión');
    }

    public function settings()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('users.settings.profile', compact('user'));
    }

    public function settingsUpdate(EditUserRequest $request)
    {
        $user = User::findOrFail(\Auth::user()->id);
        $user->fill($request->all());
        $user->save();

        //Telephone
        $telephone = UserTelephone::find(\Auth::user()->telephone->id);
        $telephone->number = $request->input('number');
        if($request->input('whatsapp') == 'yes')
        {
            $telephone->whatsapp = 1;
        }else
        {
            $telephone->whatsapp = 0;
        }
        $telephone->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function settingsAvatar()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('users.settings.avatar', compact('user'));
    }

    public function settingsAvatarUpdate()
    {
        $file = \Input::file('file');
        $validation = \Validator::make(['file'=> $file], ['file' => 'required|image|max:3000']);
        
        if(!$validation->fails())
        {
            //Rename file
            $extension = $file->getClientOriginalExtension(); 
            $newName = str_random(6).".".$extension;

            //Move file to images avatar
            $file->move('images/avatar/', $newName);

            //Update database and delete old image
            $avatar = UserAvatar::where('user_id', \Auth::user()->id)->first();
            if($avatar->route != 'default.jpg')//Nunca se elimina la default
            {
                if(\File::exists('images/avatar/'.$avatar->route)) 
                {
                    \File::delete('images/avatar/'.$avatar->route);
                } 
            }
            $avatar->route = $newName;   
            $avatar->save();

            return \Redirect::back()->with('message', 'Tu avatar se actualizó correctamente.');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

    public function settingsEmail()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('users.settings.email', compact('user'));
    }

    public function settingsEmailUpdate(EditUserEmailRequest $request)
    {
        //Actualizar usuario
        $user = User::findOrFail(\Auth::user()->id);
        $user->fill($request->all());
        $user->save();

        //Si ya estaba confirmado
        if(\Auth::user()->isConfirmed())
        {
            //Nuevo código
            $confirmation = new UserConfirmation();
            $confirmation->user_id = \Auth::user()->id;
            $confirmation->key = str_random(6);
            $confirmation->save();
        }else
        {
            $confirmation = UserConfirmation::find(\Auth::user()->confirmation->id);
            $confirmation->key = str_random(5);
            $confirmation->save();
        }

        //Email
        $subject = 'Código de confirmación';
        $to = $user->email;
        $content = 'Tu código de confirmación es: '.$confirmation->key;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function confirm_email()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('users.settings.confirmemail', compact('user'));
    }

    public function confirm_email_update()
    {
        $validation = \Validator::make(['key'=> \Request::input('key')], ['key' => 'required|max:10']);
        
        if(!$validation->fails())
        {
            if(\Auth::user()->confirmation->key === \Request::input('key'))
            {
                UserConfirmation::where('user_id',"=",\Auth::user()->id)->where('key','=',\Request::input('key'))->delete();
                return \Redirect::back();
            }else
            {
                return \Redirect::back()->with('deleteError', 'Código Incorrecto');
            }
            
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }

        return Redirect::back();
    }

    public function forwardcode()
    {
        return view('users.settings.forwardcode');
    }

    public function forwardcode_send()
    {
        $confirmation = UserConfirmation::find(\Auth::user()->confirmation->id);
        $confirmation->key = str_random(5);
        $confirmation->save();

        //Email
        $subject = 'Código de confirmación';
        $to = \Auth::user()->email;
        $content = 'Tu código de confirmación es: '.$confirmation->key;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        return \Redirect::route('confirm_email')->with('deleteError', 'Código reenviado, revisa tu email');
    }

}
