<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ad;
use App\AdEmail;
use App\AdTelephone;
use App\AdLink;
use App\AdSlug;
use App\AdDelivery;
use App\AdAttribute;
use App\AdImage;
use App\User;
use App\Category;
use App\Http\Requests\StoreNewRepublishRequest;
use App\Http\Requests\EditAdRepublishRequest;

class RepublishController extends Controller 
{
	public function create()
	{
		$user = User::findOrFail(\Auth::user()->id);

        return view('ads.add.republish', compact('user'));
	}

	public function store(StoreNewRepublishRequest $request)
	{
        $newAd = new Ad;
        $newAd->user_id = \Auth::user()->id;
        $newAd->active = 1;
        $newAd->role = 'republish';
        $newAd->type = 'ofert';
        if($request->input('price') == "")
        {
            $newAd->price = '0';
        }else
        {
            $newAd->price = $request->input('price');
        }
        $newAd->category_id = $request->input('category_id');
        $newAd->fill($request->all());
        $newAd->save();

        //Slug
        $slug = new AdSlug;
        $slug->content = str_slug($newAd->title);
        $slug->ad_id = $newAd->id;
        $slug->save();

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = new AdAttribute();
                $attribute->ad_id = $newAd->id;
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 12)//Solo 12 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(6).".".$extension;    
                        ///Move file to images/post
                        $file->move('images/ads/',$newName); 
                        //Insert in db
                        $image = new AdImage();
                        $image->ad_id = $newAd->id;
                        $image->route = $newName;
                        $image->save();    
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 12 imágenes al anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image
        
        //Email relacionado
        if($request->input('email'))
        {
            $email = new AdEmail();
            $email->ad_id = $newAd->id;
            $email->content = $request->input('email');
            $email->save();
        }

        //Telephone relacionado
        if($request->input('telephone'))
        {
            $delivery = new AdTelephone();
            $delivery->ad_id = $newAd->id;
            $delivery->content = $request->input('telephone');
            $delivery->save();
        }

        //Link relacionado
        if($request->input('link'))
        {
            $delivery = new AdLink();
            $delivery->ad_id = $newAd->id;
            $delivery->content = $request->input('link');
            $delivery->save();
        }

        //Entregas
        if($request->input('delivery'))
        {
            $delivery = new AdDelivery();
            $delivery->ad_id = $newAd->id;
            $delivery->place = $request->input('delivery');
            $delivery->save();
        }

        //Category
        $category = Category::find($request->input('category_id'));

        return \Redirect::back()->with(['id'=>$newAd->id,'category'=>$category->name,'slug'=>$slug->content]);
    }

    public function update($id, EditAdRepublishRequest $request)
    {
        $ad = Ad::findOrFail($id);
        //$ad->active = 0;
        $ad->title = $request->input('title');
        $ad->description = $request->input('description');
        $ad->price = $request->input('price');
        $ad->city_name = $request->input('city_name');

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        $attrs = $request->input('atrs_id');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = AdAttribute::find($attrs[$i]);
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Email relacionado
        if($request->input('email'))
        {
            AdEmail::where('ad_id', $ad->id)->update(['content' => $request->input('email')]);
        }

        //Telephone relacionado
        if($request->input('telephone'))
        {
            AdTelephone::where('ad_id', $ad->id)->update(['content' => $request->input('telephone')]);
        }

        //Link relacionado
        if($request->input('link'))
        {
            AdLink::where('ad_id', $ad->id)->update(['content' => $request->input('link')]);
        }

        //Delivery
        if($request->input('delivery') != "")
        {
            if($ad->delivery()->exists())
            {
                AdDelivery::where('ad_id', $ad->id)->update(['place' => $request->input('delivery')]);
            }else
            {
                $delivery = new AdDelivery;
                $delivery->ad_id = $ad->id;
                $delivery->place = $request->input('delivery');
                $delivery->save();
            }
        }

        $ad->save();

        return \Redirect::back()->with('message', 'Anuncio Actualizado');
    }

}
