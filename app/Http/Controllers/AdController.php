<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewAdRequest;
use App\Http\Requests\EditAdRequest;
use App\Ad;
use App\AdEmail;
use App\AdTelephone;
use App\AdLink;
use App\AdSlug;
use App\AdDelivery;
use App\AdAttribute;
use App\AdImage;
use App\User;
use App\UserSale;
use App\City;
use App\Category;

class AdController extends Controller
{
    public function anuncios()
    {
        return view('ads.anuncios');
    }

	public function showAd($category,$id,$slug)
    {
        $cat = new Category;
        if($cat->ifCategoryExists($category))//solo si la categoria existe
        {
            $ad = Ad::with('user')->with('images')->find($id);
            $this->notFoundUnless($ad);
            
            if($slug != $ad->slug->content)//redirigir a slug correcto
            {
                //Redirige tambien a la categoria correcta
                return \Redirect::route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]);
            }
        }else
        {
            $this->abort();//404 sino existe la categoria
        }

        return view('ads.ad', compact('ad'));
    }

    public function category($name)
    {
        $category = new Category;
        if($category->ifCategoryExists($name))
        {
            $ads = Ad::whereHas('category', function($q) use($name) {
                $q->where('name', $name);
            })->orderBy('created_at','DESC')->paginate(20);
        }else
        {
            $this->abort();//404 sino existe la categoria
        }

        return view('ads.category', compact('ads','name'));
    }

    public function mySales()
    {
        $sales = UserSale::where('seller_id',\Auth::user()->id)->where('seller_rate',0)->with('ad')->orderBy('created_at','DESC')->paginate(20);

        return view('ads.mySales', compact('sales'));
    }

    public function myPurchases()
    {
        $purchases = UserSale::where('buyer_id',\Auth::user()->id)->where('buyer_rate',0)->with('ad')->orderBy('created_at','DESC')->paginate(20);

        return view('ads.myPurchases', compact('purchases'));
    }

    public function editAd($id)
    {
        $ad = Ad::findOrFail($id);

        return view('ads.edit.editAd', compact('ad'));
    }

    public function editAdUpdate($id, EditAdRequest $request)
    {
        $ad = Ad::findOrFail($id);
        $ad->active = 0;
        $ad->title = $request->input('title');
        $ad->description = $request->input('description');
        $ad->price = $request->input('price');
        $ad->city_name = $request->input('city_name');

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        $attrs = $request->input('atrs_id');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = AdAttribute::find($attrs[$i]);
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Email relacionado
        if($request->input('email'))
        {
            AdEmail::where('ad_id', $ad->id)->update(['content' => $request->input('email')]);
        }

        //Telephone relacionado
        if($request->input('telephone'))
        {
            AdTelephone::where('ad_id', $ad->id)->update(['content' => $request->input('telephone')]);
        }

        //Delivery
        if($request->input('delivery') != "")
        {
            if($ad->delivery()->exists())
            {
                AdDelivery::where('ad_id', $ad->id)->update(['place' => $request->input('delivery')]);
            }else
            {
                $delivery = new AdDelivery;
                $delivery->ad_id = $ad->id;
                $delivery->place = $request->input('delivery');
                $delivery->save();
            }
        }

        $ad->save();

        return \Redirect::back()->with('message', 'Anuncio Actualizado');
    }

    public function myAds()
    {
        $ads = Ad::where('user_id',\Auth::user()->id)->orderBy('created_at','DESC')->paginate(20);

        return view('ads.myAds', compact('ads'));
    }

    public function destroy($id)
    {
        AdImage::find($id)->delete();

        return \Redirect::back()->with('message', 'Imágen eliminada con exito');
    }

    public function editAdImage($id)
    {
        $ad = Ad::findOrFail($id);
        $images = AdImage::where('ad_id',$id)->orderBy('created_at','DESC')->get();

        return view('ads.edit.editAdImages', compact('ad','images'));
    }

    public function editAdImageUpdate($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->active = 0;

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    

                if(!$validation->fails()) 
                {
                    //Limitando el numero de imagenes
                    if($ad->images()->count() < 12)
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(6).".".$extension;        

                        //Insert in db
                        $image = new AdImage();
                        $image->ad_id = $ad->id;
                        $image->route = $newName;
                        $image->save();        

                        ///Move file to images/post
                        $file->move('images/ads/',$newName); 
                    }else
                    {
                        return \Redirect::back()->with('image-message', 'Alcansaste el limite de 12 imágenes por anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes menores a 4MB');
                }
            }
        }else
        {
            return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
        }

        $ad->save();
        return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu anuncio']);
    }

    public function search()//Descontinuado
    {
        $query = \Request::input('query');
        $city = \Request::input('city');

        if(empty($query) && !empty($city))//Solo buscando por ciudad
        {
            $results = Ad::where('city_name','like', '%'.$city.'%')->orderBy('created_at','DESC')->paginate(20);
            $title = 'Anuncios en '.$city;
        }elseif(!empty($query) && empty($city))//Solo buscando por title
        {
            $results = Ad::where('title','like', '%'.$query.'%')->orderBy('created_at','DESC')->paginate(20);
            $title = 'Busqueda de '.$query;
        }elseif(!empty($query) && !empty($city))//Buscando con 2 parametros
        {
            $results = Ad::where('city_name','like', '%'.$city.'%')
                ->where('title','like', '%'.$query.'%')
                ->orderBy('created_at','DESC')
                ->paginate(20);
            $title = $query.' en '.$city;
        }else
        {
            $results = Ad::with('images')->orderBy('created_at','DESC')->paginate(20);
            $title = 'Todos los anuncios';
        }

        //return $results;
        return view('ads.search', compact('results','title'));
    }

    public function newAd()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('ads.add.new', compact('user'));
    }

    public function newAdStore(StoreNewAdRequest $request)
    {
        $newAd = new Ad;
        $newAd->user_id = \Auth::user()->id;
        $newAd->active = 0;
        $newAd->type = 'ofert';
        $newAd->role = 'user';
        $newAd->price = $request->input('price');
        $newAd->description = $request->input('description'); 
        $newAd->category_id = $request->input('category_id');
        $newAd->fill($request->all());
        $newAd->save();

        //Slug
        $slug = new AdSlug;
        $slug->content = str_slug($newAd->title);
        $slug->ad_id = $newAd->id;
        $slug->save();

        //Atributos
        $names = $request->input('names');
        $contents = $request->input('contents');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($names[$i]) && !empty($contents[$i]))//Solo si se envia nombre y contenido
            {  
                $attribute = new AdAttribute();
                $attribute->ad_id = $newAd->id;
                $attribute->name = $names[$i];
                $attribute->content = $contents[$i];
                $attribute->save();
            }
        }

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 12)//Solo 12 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(6).".".$extension;    
                        ///Move file to images/post
                        $file->move('images/ads/',$newName); 
                        //Insert in db
                        $image = new AdImage();
                        $image->ad_id = $newAd->id;
                        $image->route = $newName;
                        $image->save();    
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 12 imágenes al anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image

        //Email
        $subject = 'Anuncio Creado: '.$newAd->title;
        $to = \Auth::user()->email;
        $content = 'Creaste un anuncio en FijaAnuncios el anuncio tendrá un ciclo de vida de 120 días.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
            'route'=> route('anuncio', [$newAd->category->name, $newAd->id, $newAd->slug->content]),
        ];

        \Mail::send('emails.transactional.action', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        //Entregas
        if($request->input('delivery'))
        {
            $delivery = new AdDelivery();
            $delivery->ad_id = $newAd->id;
            $delivery->place = $request->input('delivery');
            $delivery->save();
        }

        $category = Category::find($request->input('category_id'));

        return \Redirect::back()->with(['id'=>$newAd->id,'category'=>$category->name,'slug'=>$slug->content]);
    }

}
