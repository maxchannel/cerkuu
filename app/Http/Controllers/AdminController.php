<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ad;
use App\AdReport;
use App\Email;
use App\User;
use App\UserReport;
use App\UserConfirmation;
use App\UserSale;
use App\UserRate;
use App\UserEmail;

class AdminController extends Controller
{
    public function newUsers()
    {
        $users = User::where('type','user')->orderBy('created_at','DESC')->paginate(20);
        $totalUsers = User::count();
        $totalEmails = UserEmail::count();
        $totalSales = UserSale::withTrashed()->count();
        $totalAds = Ad::withTrashed()->count();

        return view('admin.index', compact('users','totalUsers','totalEmails','totalSales','totalAds'));
    }

    public function statics()
    {
        $today = \Carbon::today('America/Mexico_City');
        $yesterday = \Carbon::yesterday('America/Mexico_City');

        $republish = Ad::where('role','republish')->count();
        $user = Ad::where('role','user')->count();
        $anon = Ad::where('role','anon')->count();
        //Staff1
        $Staff1 = Ad::where('role','republish')->where('user_id',62)->count();
        $Staff1Today = Ad::where('role','republish')->where('created_at','>=',$today)->where('user_id',62)->count();
        $Staff1Yes = Ad::where('role','republish')->where('created_at','>=',$yesterday)->where('user_id',62)->count();
        //Staff2
        $Staff2 = Ad::where('role','republish')->where('user_id',3)->count();
        $Staff2Today = Ad::where('role','republish')->where('created_at','>=',$today)->where('user_id',3)->count();
        $Staff2Yes = Ad::where('role','republish')->where('created_at','>=',$yesterday)->where('user_id',3)->count();

        return view('admin.statics', 
            compact('republish','user','anon','today','yesterday','Staff1','Staff1Today','Staff1Yes','Staff2','Staff2Today','Staff2Yes'));
    }

    public function newAds()
    {
        $ads = Ad::where('active',0)->orderBy('created_at','DESC')->paginate(20);

        return view('admin.newAds', compact('ads'));
    }

    public function newEmails()
    {
        $emails = Email::where('send',0)->orderBy('created_at','DESC')->paginate(20);

        return view('admin.newEmails', compact('emails'));
    }

    public function newRates()
    {
        $rates = UserRate::orderBy('created_at','DESC')->paginate(20);

        return view('admin.rates', compact('rates'));
    }

    public function expiredAds()
    {
        $ads = Ad::orderBy('created_at','ASC')->paginate(20);

        return view('admin.expired', compact('ads'));
    }

    //Metodo para boton eliminar dentro del editar
    public function deleteAdIndie($id)
    {
        //Ad::destroy($id);

        $ad = Ad::find($id);
        $ad->delete();

         //Email
        $subject = 'Anuncio Eliminado: '.$ad->title;
        $to = $ad->user->email;
        $content = 'Eliminaste exitosamente tu anuncio en FijaAnuncios';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        \Session::flash('message', 'Anuncio Eliminado');
        return \Redirect::route('myAds');
    }

    //Metodo para eliminar con ajax desde admin
    public function deleteAd($id)
    {
        $ad = Ad::findOrFail($id);

        //Move Images
        foreach($ad->images as $image) 
        {
            $old = asset('/images/ads/'.$image->route);
            $new = asset('/images/adsx/'.$image->route);

            if(\File::exists($old))
            {
                \File::move($old, $new);
            } 
        }
        //Move Images

        $ad->delete();

        //Email
        $subject = 'Anuncio Eliminado: '.$ad->title;
        $to = $ad->user->email;
        $content = 'El Anuncio se eliminó por que no cumplió con las normas de la comunidad.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function deleteEmail($id)
    {
        $email = Email::findOrFail($id);
        $email->delete();

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function activateEmail($id)
    {
        $email = Email::findOrFail($id);
        $email->send = 1;
        $email->save();

        //Email
        $subject = 'Recibiste un email';
        $to = $email->destiny;
        $content = $email->message;
        $contacto = $email->from;
        $ad_title = $email->ad->title;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
            'contacto'=>$contacto,
            'ad_title'=>$ad_title,
        ];

        \Mail::send('emails.transactional.contact', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        $message = 'Email Enviado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    //Eliminar anuncios expirados
    public function deleteExpired($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->delete();

        //Email
        $subject = 'Anuncio Eliminado: '.$ad->title;
        $to = $ad->user->email;
        $content = 'El Anuncio se eliminó por que cumplió el ciclo de 120 días activo.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    //Metodo para activar con ajax desde admin
    public function activateAd($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->active = 1;
        $ad->save();

        //Email
        $subject = 'Anuncio Aprobado: '.$ad->title;
        $to = $ad->user->email;
        $content = 'El Anuncio se aprobó, al cumplir con la normas y políticas de la comunidad.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        $message = 'Anuncio Aprobado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    //Verificar el email del usuario
    public function activateUser($id)
    {
        UserConfirmation::where('user_id',$id)->delete();
        //$confirmation->delete();

        $message = 'Anuncio Aprobado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    //Delete user
    public function deleteUser($id)
    {
        $user = User::with('avatar')->findOrFail($id);
        //Eliminando su avatar
        /*if($avatar->route != 'default.jpg')//Nunca se elimina la default
        {
            $filename = public_path().'/images/'.$user->avatar->route;
            if(\File::exists($filename)) 
            {
                \File::delete($filename);
            } 
        }*/
        //Eliminando user
        $user->delete();

        //Email
        $subject = $user->name.' fuiste expulsado';
        $to = $user->email;
        $content = 'Fuiste dado de baja de FijaAnuncios.com por 30 días, debido a faltas a las reglas y políticas de la comunidad, te enviaremos un email cuando puedas volver.';
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];

        \Mail::send('emails.email-confirmation', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function reportedAds()
    {
        $reports = AdReport::with('ad')->orderBy('created_at','DESC')->paginate(20);

        return view('admin.reportedAds', compact('reports'));
    }

    public function unreportAd($id)
    {
        $report = AdReport::findOrFail($id);
        $report->delete();

        $message = 'Anuncio Aprobado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function reportedUsers()
    {
        $reports = UserReport::with('user')->orderBy('created_at','DESC')->paginate(20);

        return view('admin.reportedUsers', compact('reports'));
    }

    public function unreportUser($id)
    {
        $report = UserReport::findOrFail($id);
        $report->delete();

        $message = 'Anuncio Aprobado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

}
