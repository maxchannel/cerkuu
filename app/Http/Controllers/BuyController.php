<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewSaleRequest;
use App\Http\Requests\StoreNewRatingRequest;
use App\Ad;
use App\UserEmail;
use App\UserSale;
use App\UserRate;
use App\UserCancelation;
use App\UserNotification;

class BuyController extends Controller
{
    public function buy($ad_id)
    {
        $ad = Ad::find($ad_id);
        $this->notFoundUnless($ad);

        return view('buys.buy', compact('ad'));
    }

    public function buyConfirm($ad_id)
    {
        $ad = Ad::find($ad_id);
        $ad->delete();

        $sale = new UserSale;
        $sale->ad_id = $ad->id;
        $sale->seller_id = $ad->user->id;
        $sale->buyer_id = \Auth::user()->id;
        $sale->seller_rate = 0;
        $sale->buyer_rate = 0;
        $sale->seller_cancel = 0;
        $sale->buyer_cancel = 0;
        $sale->save();
        
        $noti = new UserNotification;
        $noti->ad_id = $ad->id;
        $noti->user_id = $ad->user->id;
        $noti->type = 'bought';
        $noti->view = 0;
        $noti->save();

        //Email
        $subject = 'Te comprarón '.$ad->title;
        $to = $ad->user->email;
        $content = 'Te comprarón '.$ad->title;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
            'route'=> route('mySales')
        ];

        \Mail::send('emails.transactional.action', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

        });
        //Email

        return \Redirect::route('buyContact',$sale->id)->with('message', 'Compra Hecha');
    }

    public function buyRatingSeller($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        return view('buys.buyRatingSeller', compact('sale'));
    }

    public function buyRatingSellerSend($sale_id, StoreNewRatingRequest $request)
    {
        $sale = UserSale::find($sale_id);

        if($request->input('rate') > 0)
        {
            $rate = new UserRate($request->all());
            $rate->from_id = \Auth::user()->id;
            $rate->user_id = $sale->seller_id;
            $rate->save();
        }else
        {
            $sale->seller_cancel = 1;
            $cancelation = new UserCancelation($request->all());
            $cancelation->from_id = \Auth::user()->id;
            $cancelation->user_id = $sale->seller_id;
            $cancelation->save();
        }

        //Cerrando venta
        $sale->buyer_rate = 1;

        if($sale->seller_rate==1 && $sale->buyer_rate==1)//Si ya calificaron los 2
        {
            $sale->save();
            $sale->delete();
        }else
        {
            $sale->save();
        }

        return \Redirect::route('myPurchases')->with('message', 'Calificación Enviada');
    }

    public function buyRatingBuyer($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        return view('buys.buyRatingBuyer', compact('sale'));
    }

    public function buyRatingBuyerSend($sale_id, StoreNewRatingRequest $request)
    {
        $sale = UserSale::find($sale_id);

        if($request->input('rate') > 0)
        {
            $rate = new UserRate($request->all());
            $rate->from_id = \Auth::user()->id;
            $rate->user_id = $sale->buyer_id;
            $rate->save();
        }else
        {
            $sale->buyer_cancel = 1;
            $cancelation = new Cancelation($request->all());
            $cancelation->from_id = \Auth::user()->id;
            $cancelation->user_id = $sale->buyer_id;
            $cancelation->save();
        }
        
        //Cerrando venta
        $sale->seller_rate = 1;

        if($sale->seller_rate==1 && $sale->buyer_rate==1)//Si ya calificaron los 2
        {
            $sale->save();
            $sale->delete();
        }else
        {
            $sale->save();
        }

        return \Redirect::route('mySales')->with('message', 'Calificación Enviada');
    }

    public function buyContactSeller($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        return view('buys.buyContactSeller', compact('sale'));
    }

    public function buyContactSellerSend($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        $rules = ['content' => 'required'];
        $validation = \Validator::make(\Input::all(), $rules);

        if(!$validation->fails())
        {
            $subject = "El comprador de ".$sale->ad->title.' te contactó';
            $to = $sale->seller->email;
            $content = \Input::get('content');
            $contacto = $sale->buyer->email;
            $comprador = $sale->buyer->name;
            $params = [
                'subject'=>$subject,
                'content'=>$content,
                'contacto'=>$contacto,
                'comprador'=>$comprador
            ];

            \Mail::send('emails.marketing', $params, function($message) use ($subject,$to)
            {
                $message->to($to)->subject($subject);
                $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

            });

            //Email db
            $email = new UserEmail;
            $email->content = $content;
            $email->from = $contacto; 
            $email->to = $to;
            $email->sale_id = $sale->id;
            $email->save();

            return \Redirect::route('myPurchases')->with('message', 'Email enviado');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

    public function buyContactBuyer($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        return view('buys.buyContactBuyer', compact('sale'));
    }

    public function buyContactBuyerSend($sale_id)
    {
        $sale = UserSale::findOrFail($sale_id);

        $rules = ['content' => 'required'];
        $validation = \Validator::make(\Input::all(), $rules);

        if(!$validation->fails())
        {
            $subject = "El vendedor de ".$sale->ad->title.' te contactó';
            $to = $sale->buyer->email;
            $content = \Input::get('content');
            $contacto = $sale->seller->email;
            $comprador = $sale->buyer->name;
            $params = [
                'subject'=>$subject,
                'content'=>$content,
                'contacto'=>$contacto,
                'comprador'=>$comprador
            ];

            \Mail::send('emails.marketing', $params, function($message) use ($subject,$to)
            {
                $message->to($to)->subject($subject);
                $message->from('contacto@fijaanuncios.com', 'FijaAnuncios');

            });

            //Email db
            $email = new UserEmail;
            $email->content = $content;
            $email->from = $contacto; 
            $email->to = $to;
            $email->sale_id = $sale->id;
            $email->save();

            return \Redirect::route('mySales')->with('message', 'Email enviado');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

}
