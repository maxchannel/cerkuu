<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ScrapeAdRequest;
use App\Http\Requests\ScrapeAdStoreRequest;
use App\Http\Requests\StoreNewRepublishRequest;
use App\Http\Requests\MasivoAdStoreRequest;
//Fb
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphUser;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
//App
use App\Ad;
use App\AdEmail;
use App\AdTelephone;
use App\AdLink;
use App\AdSlug;
use App\AdDelivery;
use App\AdAttribute;
use App\AdImage;
use App\User;
use App\Category;

class ScraperController extends Controller 
{
    public function masivo()
    {
        session_start();
        $config = array(
            'scopes' => array('scope' => 'email','public_profile','publish_actions','user_managed_groups')
        );
        FacebookSession::setDefaultApplication(config('services.facebook.app_id'), config('services.facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(config('services.facebook.redirect'));
        try {
            $session = $helper->getSessionFromRedirect();
            if ($session):
                $_SESSION['facebook'] = $session->getToken();
                return \Redirect::route('scraper');
            endif;
            if (isset($_SESSION['facebook'])):
                $session = new FacebookSession($_SESSION['facebook']);
                $request = new FacebookRequest($session, 'GET', '/me');
                $response = $request->execute();
                $graphObjectClass = $response->getGraphObject(GraphUser::className());
                $facebook_user = $graphObjectClass;
            endif;
        } catch(FacebookRequestException $ex) {
          // When Facebook returns an error
            echo "1";
        } catch(\Exception $ex) {
          // When validation fails or other local issues
            echo "2";
        }
        //return $graphObject->getProperty('message');
        return view('admin.masivo.index', compact('helper','config','facebook_user','graphObject'));
    }

    public function masivoStore(MasivoAdStoreRequest $request)
    {
        session_start();
        FacebookSession::setDefaultApplication(config('services.facebook.app_id'), config('services.facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(config('services.facebook.redirect'));

        for($i=1; $i<=15; $i++) 
        { 
            //Ad
            $newAd = new Ad;
            $newAd->user_id = \Auth::user()->id;
            $newAd->active = 1;
            $newAd->role = 'republish';
            $newAd->type = 'ofert';
            $newAd->title = $request->input('title'.$i);
            $newAd->description = $request->input('description'.$i);
            $newAd->city_name = $request->input('city_name'.$i);
            if($request->input('price'.$i) == "")
            {
                $newAd->price = '0';
            }else
            {
                $newAd->price = \Input::get('price'.$i);
            }
            $newAd->category_id = $request->input('category_id'.$i);
            $newAd->save();
            //Slug
            $slug = new AdSlug;
            $slug->content = str_slug(str_limit($newAd->title, 100));
            $slug->ad_id = $newAd->id;
            $slug->save();
            //Email relacionado
            if($request->input('email'.$i))
            {
                $email = new AdEmail();
                $email->ad_id = $newAd->id;
                $email->content = $request->input('email'.$i);
                $email->save();
            }    
            //Telephone relacionado
            if($request->input('telephone'.$i))
            {
                $delivery = new AdTelephone();
                $delivery->ad_id = $newAd->id;
                $delivery->content = $request->input('telephone'.$i);
                $delivery->save();
            }    
            //Link relacionado
            if($request->input('link'.$i))
            {
                $delivery = new AdLink();
                $delivery->ad_id = $newAd->id;
                $delivery->content = $request->input('link'.$i);
                $delivery->save();
            }
            //Image
            $files = \Input::file('file'.$i);
            if(!empty($files[0]))//Solo si se manda 1 imagen
            {
                foreach($files as $llaves=>$file) 
                {
                    // Validate files from input file
                    $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                    $validation = \Validator::make(['file'=> $file], $rules);    
                    if(!$validation->fails()) 
                    {
                        if($llaves < 12)//Solo 12 imagenes por anuncio
                        {
                            //Rename file
                            $extension = $file->getClientOriginalExtension(); 
                            $newName = str_random(6).".".$extension;    
                            ///Move file to images/post
                            $file->move('images/ads/',$newName); 
                            //Insert in db
                            $image = new AdImage();
                            $image->ad_id = $newAd->id;
                            $image->route = $newName;
                            $image->save();    
                        }else
                        {
                            \Session::flash('image-message', 'Solo se agregaron 12 imágenes al anuncio');
                        }    
                    }else 
                    {
                        \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                    }
                }
            }
            //Image
            $ruta = 'http://fijaanuncios.com/anuncio/'.$newAd->category->name.'/'.$newAd->id.'/'.$newAd->slug->content;
            //FB
            try {
                $session = $helper->getSessionFromRedirect();
                if ($session):
                    $_SESSION['facebook'] = $session->getToken();
                    return \Redirect::route('scraper');
                endif;
                if (isset($_SESSION['facebook'])):
                    $session = new FacebookSession($_SESSION['facebook']);
                    $solicitud = new FacebookRequest(
                        $session,
                        'POST',
                        '/397241170418088/feed',
                        array (
                            'message' => '!!!ANUNCIO REPUBLICADO, click en la imágen para info',
                            'link' => $ruta,
                        )
                    );
                    $response = $solicitud->execute();
                    $graphObject = $response->getGraphObject();
                endif;
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
                echo "1";
            } catch(\Exception $ex) {
                // When validation fails or other local issues
                echo "2";
            }
            //FB
        }
        return \Redirect::route('masivo')->with('message', 'Anuncios Creados');
    }

    public function scraper()
    {
        session_start();
        $config = array(
            'scopes' => array('scope' => 'email','public_profile','publish_actions','user_managed_groups')
        );
        FacebookSession::setDefaultApplication(config('services.facebook.app_id'), config('services.facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(config('services.facebook.redirect'));
        try {
            $session = $helper->getSessionFromRedirect();
            if ($session):
                $_SESSION['facebook'] = $session->getToken();
                return \Redirect::route('scraper');
            endif;
            if (isset($_SESSION['facebook'])):
                $session = new FacebookSession($_SESSION['facebook']);
                $request = new FacebookRequest($session, 'GET', '/me');
                $response = $request->execute();
                $graphObjectClass = $response->getGraphObject(GraphUser::className());
                $facebook_user = $graphObjectClass;
            endif;
        } catch(FacebookRequestException $ex) {
          // When Facebook returns an error
            echo "1";
        } catch(\Exception $ex) {
          // When validation fails or other local issues
            echo "2";
        }
        //return $graphObject->getProperty('message');
        return view('admin.scraper.scraper', compact('helper','config','facebook_user','graphObject'));
    }
    public function scraping(ScrapeAdRequest $request)
    {
        //return \Input::get('id1').' - '.\Input::get('id2').' - '.\Input::get('id3');
        session_start();
        FacebookSession::setDefaultApplication(config('services.facebook.app_id'), config('services.facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(config('services.facebook.redirect'));
        try {
            $session = $helper->getSessionFromRedirect();
            if ($session):
                $_SESSION['facebook'] = $session->getToken();
                return \Redirect::route('scraper');
            endif;
            if (isset($_SESSION['facebook'])):
                for($i=1; $i<=10; $i++) 
                {
                    $session = new FacebookSession($_SESSION['facebook']);
                    $request = new FacebookRequest(
                    $session,
                        'GET',
                        '/'.\Input::get('id'.$i)
                    );
                    $response = $request->execute();
                    $graphObject = $response->getGraphObject();
                    $description[$i] = $graphObject->getProperty('message');
                }
            endif;
        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
            echo "1";
        } catch(\Exception $ex) {
            // When validation fails or other local issues
            echo "2";
        }
        //var_dump($description);
        return view('admin.scraper.scraping', compact('description'));
    }

    public function scraperLogout()
    {
        session_start();
        unset($_SESSION['facebook']);
        return \Redirect::route('scraper');
    }

    public function store(ScrapeAdStoreRequest $request)
    {
        session_start();
        FacebookSession::setDefaultApplication(config('services.facebook.app_id'), config('services.facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(config('services.facebook.redirect'));

        for($i=1; $i<=10; $i++) 
        { 
            //Ad
            $newAd = new Ad;
            $newAd->user_id = \Auth::user()->id;
            $newAd->active = 1;
            $newAd->role = 'republish';
            $newAd->type = 'ofert';
            $newAd->title = $request->input('title'.$i);
            $newAd->description = $request->input('description'.$i);
            $newAd->city_name = $request->input('city_name'.$i);
            if($request->input('price'.$i) == "")
            {
                $newAd->price = '0';
            }else
            {
                $newAd->price = \Input::get('price'.$i);
            }
            $newAd->category_id = $request->input('category_id'.$i);
            $newAd->save();
            //Slug
            $slug = new AdSlug;
            $slug->content = str_slug(str_limit($newAd->title, 100));
            $slug->ad_id = $newAd->id;
            $slug->save();
            //Email relacionado
            if($request->input('email'.$i))
            {
                $email = new AdEmail();
                $email->ad_id = $newAd->id;
                $email->content = $request->input('email'.$i);
                $email->save();
            }    
            //Telephone relacionado
            if($request->input('telephone'.$i))
            {
                $delivery = new AdTelephone();
                $delivery->ad_id = $newAd->id;
                $delivery->content = $request->input('telephone'.$i);
                $delivery->save();
            }    
            //Link relacionado
            if($request->input('link'.$i))
            {
                $delivery = new AdLink();
                $delivery->ad_id = $newAd->id;
                $delivery->content = $request->input('link'.$i);
                $delivery->save();
            }
            //Image
            $files = \Input::file('file'.$i);
            if(!empty($files[0]))//Solo si se manda 1 imagen
            {
                foreach($files as $llaves=>$file) 
                {
                    // Validate files from input file
                    $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                    $validation = \Validator::make(['file'=> $file], $rules);    
                    if(!$validation->fails()) 
                    {
                        if($llaves < 12)//Solo 12 imagenes por anuncio
                        {
                            //Rename file
                            $extension = $file->getClientOriginalExtension(); 
                            $newName = str_random(6).".".$extension;    
                            ///Move file to images/post
                            $file->move('images/ads/',$newName); 
                            //Insert in db
                            $image = new AdImage();
                            $image->ad_id = $newAd->id;
                            $image->route = $newName;
                            $image->save();    
                        }else
                        {
                            \Session::flash('image-message', 'Solo se agregaron 12 imágenes al anuncio');
                        }    
                    }else 
                    {
                        \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                    }
                }
            }
            //Image
            $ruta = 'http://fijaanuncios.com/anuncio/'.$newAd->category->name.'/'.$newAd->id.'/'.$newAd->slug->content;
            //FB
            try {
                $session = $helper->getSessionFromRedirect();
                if ($session):
                    $_SESSION['facebook'] = $session->getToken();
                    return \Redirect::route('scraper');
                endif;
                if (isset($_SESSION['facebook'])):
                    $session = new FacebookSession($_SESSION['facebook']);
                    $solicitud = new FacebookRequest(
                        $session,
                        'POST',
                        '/397241170418088/feed',
                        array (
                            'message' => '!!!ANUNCIO REPUBLICADO, click en la imágen para info',
                            'link' => $ruta,
                        )
                    );
                    $response = $solicitud->execute();
                    $graphObject = $response->getGraphObject();
                endif;
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
                echo "1";
            } catch(\Exception $ex) {
                // When validation fails or other local issues
                echo "2";
            }
            //FB
        }
        return \Redirect::route('scraper')->with('message', 'Anuncios Creados');
    }
}