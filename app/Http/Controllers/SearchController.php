<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ad;
use App\Category;
use App\Search;
use App\SearchCity;
use App\SearchUser;

class SearchController extends Controller 
{
    public function index()
    {
        return view('search.index');
    }

    public function search()
    {
        $query = \Request::input('query');
        $description = \Request::input('description');
        $city = \Request::input('city');
        $price_start = \Request::input('price_start');
        $price_end = \Request::input('price_end');
        $category_id = \Request::input('category_id');
        $sort = \Request::input('sort');
        $title = 'Anuncios';

        $consulta = Ad::orderBy('created_at', 'DESC');

        if($sort)
        {
            if($sort == 'price')
            {
                $consulta = Ad::orderBy('price', 'DESC');
            }elseif($sort == 'price_a')
            {
                $consulta = Ad::orderBy('price', 'ASC');
            }elseif($sort == 'date')
            {
                $consulta = Ad::orderBy('created_at', 'DESC');
            }elseif($sort == 'date_a')
            {
                $consulta = Ad::orderBy('created_at', 'ASC');
            }
        }

        if($category_id)
        {
            $cate = Category::find($category_id);
            $this->notFoundUnless($cate);
            $consulta->where('category_id', $category_id);
            $title .= ' en '.trans('categories.'.$cate->name);
        }

        if($query)
        {
            $consulta->where('title','like', '%'.$query.'%');
            $title = $query;

            //Guardando la busqueda
            $search2 = new Search;
            $search2->query = $query;
            $search2->save();

            if($city)
            {
                $city2 = new SearchCity;
                $city2->city = $city;
                $city2->search_id = $search2->id;
                $city2->save();
            }

            if(\Auth::check() && \Auth::user()->type == 'user')
            {
                $user2 = new SearchUser;
                $user2->user_id = \Auth::user()->id;
                $user2->search_id = $search2->id;
                $user2->save();
            }
            //Guardando la busqueda
        }

        if($description)
        {
            $consulta->where('description','like', '%'.$description.'%');
            $title .= ' con '.$description.' en la descripción ';
        }

        if($city)
        {
            $consulta->where('city_name','like', '%'.$city.'%');
            $title .= ' en '.$city;
        }

        if($price_start)
        {
            $consulta->where('price', '>', $price_start);
        }

        if($price_end)
        {
            $consulta->where('price', '<', $price_end);
        }

        $results = $consulta->paginate(30);
        return view('search.search', compact('results', 'sort', 'title'));
    }
}
