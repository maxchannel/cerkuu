<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdTelephone extends Model 
{
	use SoftDeletes;

	protected $table = 'ad_telephones';
	protected $dates = ['deleted_at'];

	public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
}
