<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model 
{
	use SoftDeletes;

	protected $table = 'jobs';
    protected $fillable = ['company','title','description','city_name','telephone','email','education','salary','lang'];
    protected $dates = ['deleted_at'];
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function attributes()
    {
        return $this->hasMany('App\JobAttribute','job_id','id');
    }

    public function slug()
    {
        return $this->hasOne('App\JobSlug','job_id','id');
    }

}
