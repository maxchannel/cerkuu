<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmail extends Model
{
    protected $table = 'user_emails_sended';
    protected $fillable = ['content', 'user_id', 'from_id'];

    public function sale()
    {
        return $this->belongsTo('App\UserSale');
    }

}
