<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdName extends Model 
{
	use SoftDeletes;

	protected $table = 'ad_names';
	protected $dates = ['deleted_at'];

	public function ad()
    {
        return $this->belongsTo('App\Ad');
    }

}
