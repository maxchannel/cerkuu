<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotification extends Model
{
    use SoftDeletes;
    protected $table = 'user_notifications';
	protected $fillable = ['type','ad_id','user_id','view'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function ad()
    {
        return $this->belongsTo('App\Ad')->withTrashed();
    }
}
