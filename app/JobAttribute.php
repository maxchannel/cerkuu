<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAttribute extends Model 
{
	protected $table = 'job_attributes';
	protected $fillable = ['name', 'content'];

    public function job()
    {
        return $this->belongsTo('App\Job');
    }

}
