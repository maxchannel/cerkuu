<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchUser extends Model 
{
	use SoftDeletes;

    protected $fillable = ['user_id', 'search_id'];
    protected $dates = ['deleted_at'];

    public function search()
    {
        return $this->belongsTo('App\Search');
    }

}
