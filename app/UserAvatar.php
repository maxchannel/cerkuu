<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAvatar extends Model
{
	use SoftDeletes;
    protected $table = 'user_avatars';
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function findByUserID($user_id)
	{
		return $this->model->where('user_id', '=', $user_id)->first();
	}
}
