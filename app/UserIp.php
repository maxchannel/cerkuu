<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserIp extends Model
{
	use SoftDeletes;
	
    protected $table = 'user_ips';
    protected $dates = ['deleted_at'];

}
