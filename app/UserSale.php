<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSale extends Model
{
    use SoftDeletes;

    protected $table = 'user_sales';
    protected $fillable = ['ad_id', 'seller_id', 'buyer_id'];
    protected $dates = ['deleted_at'];

	public function ad()
    {
        return $this->hasOne('App\Ad','id','ad_id')->withTrashed();
    }

    public function seller()
    {
        return $this->hasOne('App\User','id','seller_id');
    }

    public function buyer()
    {
        return $this->hasOne('App\User','id','buyer_id');
    }

}
