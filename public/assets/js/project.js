$(document).ready(function() {
    var count = 2;
    var maxCounts = 8;

    //hide if not empty
    for(var i=2; i<=8; i++) 
    {
        if($('#nm'+i).val() == "") 
        {
            $('#cp'+i).hide();
        }else
        {
            count++;//solo incrementar cuando no es vacio
        }
    };

    $('#addAttr').click(function(){
        
        if(count <= maxCounts)
        {
            $('#cp'+count).show();
            count++;
        }

    });

});