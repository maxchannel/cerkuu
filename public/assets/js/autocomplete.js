$(function()
{
     $( "#city" ).autocomplete({
      source: "http://localhost/cerkuu/public/api/cities",
      minLength: 2,
      select: function(event, ui) {
        $('#q').val(ui.item.value);
        //Append del id de la ciudad en un input hidden
        $('#formulario').append("<input type='hidden' name='city_id' value='"+ui.item.id+"' />"); 
      }
    });
});