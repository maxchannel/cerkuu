<?php

return [
    'categories' => [
        '' => 'Seleccionar Categoría',
        'Gadgets' => [
            '1' => 'Smartphone', 
            '9' => 'Tablet',
            '4' => 'Laptop y PC',
            '10' => 'Fotografía', 
            '22' => 'Alacenamiento',
            ],
        'Vehículos' => [
            '2'=>'Autos',
            '3'=>'Motocicletas',
            '19' => 'Autopartes y Refacciones',
            '27' => 'Llantas y Rines',
            '26' => 'Bicicletas',
            ],
        'Hobby' => [
            '23' => 'Deportes',
            '24' => 'Juguetes',
            '25' => 'Instrumentos',
            '8' => 'Videojuegos', 
            '20' => 'Audio',
            ],
        'Hogar' => [
            '12'=>'Electrodomésticos',
            '13'=>'Muebles',
            '21' => 'Pantallas',
            '7' => 'Electrónica',
            '15' => 'Servicio',
            '16' => 'Accesorio',
            ],
        'Inmuebles' => [
            '5'=>'Casas',
            '6'=>'Terrenos',
            '29' => 'Oficinas y Locales',
            '28' => 'Departamentos',
            ],
        'Estilos' => [
            '17' => 'Ropa y Zapatos',
            '11' => 'Personal', 
            '14' => 'Mascotas',
            '18' => 'Belleza',
            ],
        '30' => 'Otro',
    ]
];
        
