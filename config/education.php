<?php

return [
       'level' => [
            '' => 'Selecciona Educación',
            'basic' => 'Primaria y Secundaria',
            'junior' => 'Bachillerato',
            'college' => 'Universidad',
            'master' => 'Maestría',
            'doctor' => 'Doctorado'
        ] 
        
];