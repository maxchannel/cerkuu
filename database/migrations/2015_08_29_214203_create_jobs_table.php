<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('company');
            $table->string('title');
            $table->text('description');
            $table->string('city_name');
            $table->string('telephone');
            $table->string('email');
            $table->string('education');
            $table->string('salary');
            $table->string('lang');
            $table->boolean('active');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
        });
    }
    
    public function down()
    {
        Schema::drop('jobs');
    }
}
