<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdReportsTable extends Migration 
{

	public function up()
	{
		Schema::create('ad_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ad_id')->unsigned();
			$table->string('name');
			$table->integer('report');
			$table->string('email');
			$table->string('telephone');
			$table->string('comments');
			$table->timestamps();

			$table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('ad_reports');
	}

}
