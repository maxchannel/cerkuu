<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAttributesTable extends Migration 
{
	public function up()
    {
        Schema::create('job_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->string('name');
            $table->string('content');
            $table->timestamps();
            
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('job_attributes');
    }
}