<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration 
{

	public function up()
	{
		Schema::create('searches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('query');
			$table->timestamps();

			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('searches');
	}

}
