<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchCitiesTable extends Migration 
{

	public function up()
	{
		Schema::create('search_cities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('city');
			$table->integer('search_id')->unsigned();
			$table->timestamps();

			$table->softDeletes();
			$table->foreign('search_id')->references('id')->on('searches')->onDelete('cascade');;
		});
	}

	public function down()
	{
		Schema::drop('search_cities');
	}

}
