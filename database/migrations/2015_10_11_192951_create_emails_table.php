<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration 
{
	public function up()
	{
		Schema::create('emails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('message');
			$table->string('from');
			$table->string('destiny');
			$table->boolean('send');
			$table->integer('ad_id')->unsigned();
			$table->timestamps();

			$table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
			$table->softDeletes();
		});
	}


	public function down()
	{
		Schema::drop('emails');
	}

}
