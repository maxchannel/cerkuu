<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchUsersTable extends Migration 
{

	public function up()
	{
		Schema::create('search_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('search_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();

			$table->softDeletes();
			$table->foreign('search_id')->references('id')->on('searches');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	public function down()
	{
		Schema::drop('search_users');
	}

}
