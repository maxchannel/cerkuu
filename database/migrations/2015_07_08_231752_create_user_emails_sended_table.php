<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEmailsSendedTable extends Migration
{
    public function up()
    {
        Schema::create('user_emails_sended', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->string('from');
            $table->string('to');
            $table->integer('sale_id')->unsigned();
            $table->timestamps();

            $table->foreign('sale_id')->references('id')->on('user_sales');

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_emails_sended');
    }
}
