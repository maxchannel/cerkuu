<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSalesTable extends Migration 
{
	public function up()
    {
        Schema::create('user_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ad_id')->unsigned();
            $table->integer('seller_id')->unsigned();
            $table->integer('buyer_id')->unsigned();
            $table->boolean('seller_rate');
            $table->boolean('buyer_rate');
            $table->boolean('seller_cancel');
            $table->boolean('buyer_cancel');
            $table->timestamps();
            
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('seller_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_sales');
    }
}
