<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSlugsTable extends Migration 
{
	public function up()
    {
        Schema::create('job_slugs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('job_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('job_slugs');
    }
}
