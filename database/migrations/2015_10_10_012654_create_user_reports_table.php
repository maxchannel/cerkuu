<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReportsTable extends Migration 
{

	public function up()
	{
		Schema::create('user_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('name');
			$table->integer('report');
			$table->string('email');
			$table->string('telephone');
			$table->string('comments');
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_reports');
	}

}
