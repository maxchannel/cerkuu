<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRatesTable extends Migration 
{
	public function up()
    {
        Schema::create('user_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rate')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('from_id')->unsigned();
            $table->string('comments');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('from_id')->references('id')->on('users')->onDelete('cascade');

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_rates');
    }
}
