<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdSlugsTable extends Migration 
{

	public function up()
    {
        Schema::create('ad_slugs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('ad_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('ad_slugs');
    }

}
