<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call('CategoryTableSeeder');
        $this->call('AdminTableSeeder');
        $this->call('UserTableSeeder');

        Model::reguard();
    }
}
