<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Ad;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        $id = \DB::table('users')->insertGetId( array(
        	'name'=>'Max',
        	'email'=>'max_jal@hotmail.com',
        	'username'=>'max',
        	'password'=>\Hash::make('secret'),
            'type'=>'admin',
        ));

        //Avatar
        \DB::table('user_avatars')->insert( array(
            'user_id'=>$id,
            'route'=> 'default.jpg'
        ));

        //Telephone
        \DB::table('user_telephones')->insert( array(
            'user_id'=>$id,
            'number'=> 3316013779,
            'whatsapp'=> $faker->randomElement([0,1]),
        ));

        //Anuncio Anonimo
        $ad_id = Ad::create([
            'user_id'=>$id,
            'title'=> $faker->text(40),
            'description'=> $faker->text(200),
            'price'=>$faker->randomNumber(3),
            'city_name'=>'Guadalajara',
            'role'=>'anon',
            'type'=>'ofert',
            'category_id'=>1,
            'active'=>0,
            'created_at'=>'2015-06-12 04:13:36'
        ]);


        \DB::table('ad_keys')->insert( array(
            'content'=> str_random(5),
            'ad_id'=> $ad_id->id
        ));

        \DB::table('ad_slugs')->insert( array(
            'content'=> str_slug($ad_id->title),
            'ad_id'=> $ad_id->id
        ));

        \DB::table('ad_images')->insert( array(
            'route'=> $faker->randomElement(['a.png','b.png','c.png','d.png','e.png']),
            'ad_id'=> $ad_id->id
        ));

        \DB::table('ad_emails')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => 'maxinmars@gmail.com',
        ));

        \DB::table('ad_names')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => 'Max',
        ));

        \DB::table('ad_telephones')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => '331601',
        ));
        //Anuncio Anonimo

        //Anuncio Republicado
        $ad_id = Ad::create([
            'user_id'=>$id,
            'title'=> $faker->text(100),
            'description'=> $faker->text(200),
            'price'=>$faker->randomNumber(3),
            'city_name'=>'Guadalajara',
            'role'=>'republish',
            'type'=>'ofert',
            'category_id'=>1,
            'active'=>0,
            'created_at'=>'2015-06-12 04:13:36'
        ]);


        \DB::table('ad_slugs')->insert( array(
            'content'=> str_slug($ad_id->title),
            'ad_id'=> $ad_id->id
        ));

        \DB::table('ad_images')->insert( array(
            'route'=> $faker->randomElement(['a.png','b.png','c.png','d.png','e.png']),
            'ad_id'=> $ad_id->id
        ));

        \DB::table('ad_emails')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => 'a@gmail.com',
        ));

        \DB::table('ad_links')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => 'http://www.google.com',
        ));

        \DB::table('ad_telephones')->insert( array(
            'ad_id'=> $ad_id->id,
            'content' => '331601',
        ));
        //Anuncio Republicado

    }
}
