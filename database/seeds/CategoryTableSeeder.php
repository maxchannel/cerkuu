<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        //Categorias
        Category::create(array('name' => 'smartphone'));
        Category::create(array('name' => 'autos'));
        Category::create(array('name' => 'motocicletas'));
        Category::create(array('name' => 'pc'));
        Category::create(array('name' => 'casas'));//Antes inmuebles
        Category::create(array('name' => 'terrenos'));//Antes hogar
        Category::create(array('name' => 'electronics'));
        Category::create(array('name' => 'videogames'));
        Category::create(array('name' => 'tablet'));
        Category::create(array('name' => 'photography'));
        Category::create(array('name' => 'personal'));
        Category::create(array('name' => 'electrodomesticos'));
        Category::create(array('name' => 'muebles'));
        Category::create(array('name' => 'mascotas'));
        Category::create(array('name' => 'servicios'));
        Category::create(array('name' => 'accessory'));
        Category::create(array('name' => 'clothes'));
        Category::create(array('name' => 'belleza'));
        Category::create(array('name' => 'autopartes'));//Antes other
        Category::create(array('name' => 'audio'));
        Category::create(array('name' => 'tv'));
        Category::create(array('name' => 'storage'));
        Category::create(array('name' => 'sports'));//Antes hobby
        Category::create(array('name' => 'juguetes'));
        Category::create(array('name' => 'instrumentos'));
        //Nuevas 25 + 5
        Category::create(array('name' => 'bicicletas'));
        Category::create(array('name' => 'llantas'));
        Category::create(array('name' => 'departamentos'));
        Category::create(array('name' => 'oficinas'));
        Category::create(array('name' => 'other'));
    }
}
