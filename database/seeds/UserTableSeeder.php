<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Ad;


class UserTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        foreach(range(1, 40) as $index)//Minimo 6 sino truena
        {
            //Users
            $id = \DB::table('users')->insertGetId( array(
                'name'=> $faker->name,
                'email'=> $faker->email,
                'username'=> $faker->username,
                'password'=>\Hash::make('secret'),
                'type'=>'user',
                'created_at'=>$faker->randomElement(['2015-06-12 04:13:36','2015-06-28 04:13:36','2015-03-15 04:13:36'])
            ));

            //Avatar
            \DB::table('user_avatars')->insert( array(
                'user_id'=>$id,
                'route'=> 'default.jpg'
            ));

            //Telephone
            \DB::table('user_telephones')->insert( array(
                'user_id'=>$id,
                'number'=> 3316013779,
                'whatsapp'=> $faker->randomElement([0,1]),
            ));

            //Confirmation
            \DB::table('user_confirmations')->insert( array(
                'user_id'=>$id,
                'key'=> str_random(5)
            ));

            //Anuncios
            $ad_id = Ad::create([
                'user_id'=>$id,
                'title'=> $faker->text(40),
                'description'=> $faker->text(500),
                'price'=>$faker->randomElement([0, $faker->randomNumber(4)]),
                'city_name'=>$faker->randomElement(['Guadalajara','Zapopan','Tlaquepaque']),
                'category_id'=>$faker->randomElement([1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]),
                'role'=>'user',
                'type'=>'ofert',
                'active'=>$faker->randomElement([0,1]),
                'created_at'=>$faker->randomElement(['2015-06-12 04:13:36','2015-06-28 04:13:36','2015-03-15 04:13:36'])
            ]);

            \DB::table('ad_slugs')->insert( array(
                'content'=> str_slug($ad_id->title),
                'ad_id'=> $ad_id->id
            ));

            \DB::table('ad_images')->insert( array(
                'route'=> $faker->randomElement(['a.png','b.png','c.png','d.png','e.png']),
                'ad_id'=> $ad_id->id
            ));

            \DB::table('ad_images')->insert( array(
                'route'=> $faker->randomElement(['a.png','b.png','c.png','d.png','e.png']),
                'ad_id'=> $ad_id->id
            ));

            \DB::table('ad_attributes')->insert( array(
                'name' => 'marca',
                'content' => 'apple',
                'ad_id' => $ad_id->id
            ));

            \DB::table('ad_deliveries')->insert( array(
                'ad_id' => $ad_id->id,
                'place' => $faker->text(35)
            ));
            
        }

        /*\DB::table('sales')->insert( array(
            'ad_id' => 1,
            'seller_id' => 1,
            'buyer_id' => 2,
            'created_at'=>'2015-06-12 04:13:36'
        ));    

        \DB::table('sales')->insert( array(
            'ad_id' => 2,
            'seller_id' => 3,
            'buyer_id' => 1,
            'created_at'=>'2015-06-12 04:13:36'
        ));

        \DB::table('sales')->insert( array(
            'ad_id' => 3,
            'seller_id' => 3,
            'buyer_id' => 5,
            'created_at'=>'2015-06-12 04:13:36'
        ));    

        \DB::table('sales')->insert( array(
            'ad_id' => 4,
            'seller_id' => 7,
            'buyer_id' => 6,
            'created_at'=>'2015-06-12 04:13:36'
        ));

        \DB::table('notifications')->insert( array(
            'type' => 'bought',
            'ad_id' => 1,
            'user_id' => 1,
            'created_at'=>'2015-06-12 04:13:36'
        ));

        \DB::table('notifications')->insert( array(
            'type' => 'welcome',
            'ad_id' => 1,
            'user_id' => 1,
            'created_at'=>'2015-06-12 04:13:36'
        ));*/

        \DB::table('user_cancelations')->insert( array(
            'user_id'=>2,
            'from_id'=> 3
        ));

        \DB::table('user_notifications')->insert( array(
            'type'=>'bought',
            'user_id'=> 2,
            'ad_id'=> 1,
            'view'=> 1
        ));

        \DB::table('ad_reports')->insert( array(
            'ad_id'=>1,
            'name'=>'Max',
            'report'=> 1,
            'comments'=>'hello',
            'email'=>'max@hotmail.com',
            'telephone'=>'65245624',
        ));

        \DB::table('ad_reports')->insert( array(
            'ad_id'=>1,
            'name'=>'Max',
            'report'=> 3,
            'comments'=>'hola',
            'email'=>'mar@hotmail.com',
            'telephone'=>'65245624',
        ));

        \DB::table('user_reports')->insert( array(
            'user_id'=>3,
            'name'=>'Max',
            'telephone'=>'65245624',
            'report'=> 1,
            'comments'=>'hello',
            'email'=>'max@hotmail.com'
        ));

        \DB::table('user_reports')->insert( array(
            'user_id'=>2,
            'name'=>'Max',
            'telephone'=>'65245624',
            'report'=> 3,
            'comments'=>'hola',
            'email'=>'mar@hotmail.com'
        ));

    }

    
}
