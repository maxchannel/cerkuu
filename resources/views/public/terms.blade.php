@extends('layouts.master')

@section('title', 'Términos y condiciones')
@section('meta-description', 'Términos y condiciones')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Términos y condiciones</h1>
                <hr>
                <h3>¿Se hace FijaAnuncios responsable de lo que se vende en la página?</h3>
                <p>No. Los compradores y vendedores son los responsables de lo que se vende en FijaAnuncios.</p>
                <p>Compradores: las transacciones realizadas a través de la página de FijaAnuncios son entre el vendedor y tú. Ten en cuenta que FijaAnuncios no posee ninguno de los artículos que oferta un vendedor ni es propietario de estos, por lo que, si tienes alguna pregunta relacionada con lo que vas a comprar en la página de FijaAnuncios, intenta ponerte en contacto con el vendedor.</p>
                <p>Vendedores: describe claramente los artículos que vendes y asegúrate de cumplir nuestra Declaración de derechos y responsabilidades y nuestras Normas comunitarias.</p>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection