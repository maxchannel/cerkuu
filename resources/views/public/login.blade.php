@extends('layouts.master')

@section('title', 'Login')
@section('meta-description', 'Login')

@section('content')
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <h2 class="form-signin-heading">Iniciar Sesión</h2> 

            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif
            @include('partials.errorMessages')

            {!! Form::open(['route'=>'loginSend', 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::text('email',null,['class'=>'form-control', 'placeholder'=>'Email']) !!}
                {!! Form::password('password',['class'=>'form-control', 'placeholder'=>'Password']) !!}
         
                <div class="checkbox">
                    <label class="remember">
                        {!! Form::checkbox('remember') !!} Recordarme
                    </label>
                </div>    

                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>    
            {!! Form::close() !!}

            @if(Session::has('login_error'))
                </br><div class="alert alert-danger" role="alert"><p>Email o Password incorrectos</p></div>
            @endif

            </br><p class="text-muted">Eres nuevo? <a href="{{ route('signup') }}">Registrate</a></p>

        </div>
        <div class="col-md-4">
        </div>
    </div>
@endsection