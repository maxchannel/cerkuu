@extends('layouts.master')

@section('title', 'FijaAnuncios')
@section('meta-description', 'FijaAnuncios')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
{!! HTML::style('assets/css/jumbotron.css') !!}
{!! HTML::style('assets/css/jquery-ui.css') !!}
<link href='https://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>
            
            <!-- Anuncios -->
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <div class="row">
                    @if($ads->count() > 0)
                        <p class="pull-right">
                                @if(\Request::input('sort') == 'date' || \Request::input('sort') == 'date_a')
                                Fecha
                                @else
                                <a href="{{ route('home', ['sort'=>'date']) }}">Fecha</a>
                                @endif
                                /
                                @if(\Request::input('sort') == 'price' || \Request::input('sort') == 'price_a')
                                Precio
                                @else
                                <a href="{{ route('home', ['sort'=>'price']) }}">Precio</a>
                                @endif

                                @if(\Request::input('sort') == 'price')
                                <a href="{{ route('home', ['sort'=>'price_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                                @elseif(\Request::input('sort') == 'price_a')
                                <a href="{{ route('home', ['sort'=>'price']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                                @endif    

                                @if(\Request::input('sort') == 'date')
                                <a href="{{ route('home', ['sort'=>'date_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                                @elseif(\Request::input('sort') == 'date_a')
                                <a href="{{ route('home', ['sort'=>'date']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                                @endif
                        </p>    
                    @else
                    <h3>Sin Resultados</h3>
                    @endif
                </div>

                <div class="row">
                <section id="pinBoot">
                    @if(\Request::input('page') == 1 || \Request::input('page') == "")
                    <article class="white-panel"> 
                        <h4>Ciudades populares</h4>
                        <a href="">Guadalajara</a><br>
                        <a href="">Zapopan</a><br>
                    </article>
                    @endif
                @foreach($ads as $ad)
                    <article class="white-panel"> 
                        <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}"><img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="{{ $ad->title }}"></a> <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}"></a>
                        <h4><a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">{{ ucfirst($ad->title) }}</a></h4>
                        <p>
                            @if($ad->price > 0)
                            {{ number_format($ad->price) }} $
                            @else
                            Sin precio
                            @endif
                        </p>
                        <p>
                            <script>
                            moment.locale("es");
                            document.writeln(moment.utc("{{ $ad->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                            </script>
                        </p>
                    </article>
                @endforeach
                </section>
                </div>          
                {!! $ads->appends(Request::only('sort'))->render() !!}      
            <!-- Anuncios -->

            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>  
        </div>

    </div><!--/.container-->
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
<script src="{{ asset('assets/js/image.js') }}"></script>
@endsection
