<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h1>{{ $subject }}</h1>

		<div>
			<p>{{ $content }}</p>

			<p><a href="http://fijaanuncios.com">FijaAnuncios.com</a></p>
		</div>
	</body>
</html>