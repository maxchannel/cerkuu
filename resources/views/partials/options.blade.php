                <select class="form-control" name="category_id">
                    <option value="{{ Input::old('category_id') }}" selected="selected">Seleccionar Categor&iacute;a</option>
                    <optgroup label="Gadgets">
                        <option value="1">Smartphone</option>
                        <option value="9">Tablet</option>
                        <option value="4">Laptop y PC</option>
                        <option value="10">Fotografía</option>
                        <option value="22">Alacenamiento</option>
                    </optgroup>
                    <optgroup label="Vehículos">
                        <option value="2">Autos</option>
                        <option value="3">Motocicletas</option>
                        <option value="26">Bicicletas</option>
                        <option value="27">Llantas y Rines</option>
                        <option value="19">Autopartes y Refacciones</option>
                    </optgroup>
                    <optgroup label="Hobby">
                        <option value="23">Deportes</option>
                        <option value="24">Juguetes</option>
                        <option value="25">Instrumentos</option>
                        <option value="8">Videojuegos</option>
                        <option value="20">Audio</option>
                    </optgroup>
                    <optgroup label="Hogar">
                        <option value="12">Electrodomésticos</option>
                        <option value="13">Muebles</option>
                        <option value="7">Electrónica</option>
                        <option value="21">Pantallas</option>
                        <option value="15">Servicio</option>
                        <option value="16">Accesorio</option>
                    </optgroup>
                    <optgroup label="Inmuebles">
                        <option value="5">Casas</option>
                        <option value="28">Departamentos</option>
                        <option value="6">Terrenos</option>
                        <option value="29">Oficinas y Locales</option>
                    </optgroup>
                    <optgroup label="Estilos">
                        <option value="17">Ropa y Zapatos</option>
                        <option value="18">Belleza</option>
                        <option value="11">Personal</option>
                        <option value="14">Mascotas</option>
                    </optgroup>
                    <option value="30">Otro</option>
                </select>
