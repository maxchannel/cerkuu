@extends('layouts.master')

@section('title', 'Editar Vacante')
@section('meta-description', 'Editar Vacante')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')

    <div class="container min-height">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <h1>Editar Vacante</h1>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::model($job,['route'=>['editJobUpdate', $job->id], 'method'=>'PUT', 'role'=>'form']) !!}
                {!! Form::label('company', 'Empresa*') !!}
                {!! Form::text('company',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Empresa']) !!}
                {!! Form::label('title', 'Titulo*') !!}
                {!! Form::text('title',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Titulo de puesto']) !!}
                {!! Form::label('description', 'Descripción*') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Cuenta detalles de la vacante']) !!}
                {!! Form::label('city_name', 'Donde* (Nombre de la Ciudad) ') !!}
                {!! Form::text('city_name',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                {!! Form::label('telephone', 'Telefono*') !!}
                {!! Form::text('telephone',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Telefono']) !!}
                {!! Form::label('email', 'Email*') !!}
                {!! Form::text('email',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Email']) !!}
                {!! Form::label('education', 'Educación*') !!}
                {!! Form::select('education',config('education.level') ,null,['class'=>'form-control']) !!}
                </br><button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
            <div class="col-md-4">
                <h2>Opcional</h2>
                {!! Form::label('salary', 'Salario') !!}
                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Salario (en pesos)</label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        {!! Form::number('salary',null,['class'=>'form-control', 'maxlength'=>'15', 'id'=>'exampleInputAmount','placeholder'=>'Salario (en pesos)']) !!}
                        <div class="input-group-addon">.00</div>
                    </div>
                </div> 
                {!! Form::label('lang', 'Idioma') !!}
                {!! Form::text('lang',null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Si aplica, escribe un segundo idioma']) !!}
                
                @if($job->attributes()->exists())
                <h3>Atributos</h3>
                <p class="text-muted">Información relevante, ej. Marca, Año, Modelo, etc.</p>
                <div class="form-inline">
                    @foreach($job->attributes as $key=>$attribute)
                        <div>
                            <div class="form-group">
                                {!! Form::text('names['.$key.']',$attribute->name,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::text('contents['.$key.']',$attribute->content,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                                {!! Form::hidden('atrs_id['.$key.']', $attribute->id) !!}
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif

            {!! Form::close() !!}

            {!! Form::open(['route'=>['deleteAdIndie', $job->id], 'method'=>'DELETE']) !!}
            </br><button type="submit" class="btn btn-danger" onclick="return confirm('¿Confirma que realmente quiere ELIMINAR EL ANUNCIO?')" ><span class="glyphicon glyphicon-trash"></span> Eliminar Vacante</button>
            {!! Form::close() !!}
            </div>
        </div>
    </div> <!-- /container -->

@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
<script src="{{ asset('assets/js/project.js') }}"></script>
@endsection

