@extends('layouts.master')

@section('title'){{ $job->title }}@endsection
@section('meta-description'){{ $job->title }}@endsection

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('script_body')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection

@section('content')

    <div class="container min-height">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-7">
                <h1 class="word-wrap">{{ $job->title }}</h1>

                <!-- Imágen -->
                @if(count($job->images))
                <div class="bs-example" data-example-id="simple-carousel">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach($job->images as $key => $image)
                                    @if($key == 0)
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                                    @else
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                                    @endif
                                @endforeach  
                            </ol>

                            <div class="carousel-inner" role="listbox">
                                @foreach($job->images as $key => $image)
                                    @if($key == 0)
                                        <div class="item active">
                                            <img src="../../../images/ads/{{ $image->route }}" class="tales" alt="{{ $job->title }}">
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="../../../images/ads/{{ $image->route }}" class="tales" alt="{{ $job->title }}">
                                        </div> 
                                    @endif
                                @endforeach  
                            </div>

                            @if(count($job->images) > 1)
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            @endif
                      </div>
                </div>
                @endif
                <!-- Imágen -->
                
                </br>
                <dl class="dl-horizontal">
                    <dt>Empresa</dt>
                    <dd>{{ $job->company }}</dd>
                    <dt>Ubicación</dt>
                    <dd><a href="{{ route('search', ['city'=>$job->city_name]) }}">{{ ucfirst($job->city_name) }}</a></dd>
                    <dt>Publicado por</dt>
                    <dd>
                        <a href="{{ route('user',[$job->user->id]) }}">{{ $job->user->name }}</a> 
                    </dd>
                    @if($job->salary != "")
                    <dt>Sueldo</dt>
                    <dd>
                        {{ $job->salary }}
                    </dd>
                    @endif
                    @if($job->lang != "")
                    <dt>Idioma</dt>
                    <dd>
                        {{ $job->lang }}
                    </dd>
                    @endif
                    @if($job->email != "")
                    <dt>Email</dt>
                    <dd>
                        {{ $job->email }}
                    </dd>
                    @endif
                    @if($job->telephone != "")
                    <dt>Telefono</dt>
                    <dd>
                        {{ $job->telephone }}
                    </dd>
                    @endif


                    @if(count($job->attributes))
                        @foreach($job->attributes as $attribute)
                            <dt>{{ ucfirst($attribute->name) }}</dt>
                            <dd>{{ ucfirst($attribute->content) }}</dd>
                        @endforeach
                    @endif
                    </br>
                    <dt></dt>
                    <dd>
                        @if(Auth::check())
                            @if($job->user->id == Auth::user()->id)
                            <a href="{{ route('editJob', [$job->id]) }}" type="button" class="btn btn-warning">Editar</a> <a href="{{ route('editAd', [$job->id]) }}" type="button" class="btn btn-danger">Eliminar</a>
                            @endif
                        @endif
                    </dd>
                    <dt>Publicado</dt>
                    <dd>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $job->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </dd>
                    <dt>Descripción</dt>
                    <dd>{{ $job->description }}</dd>
                    </br>
                    <dt>Compartir</dt>
                    <dd>
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"></div>
                        <div class="fb-send" data-href="https://developers.facebook.com/docs/plugins/"></div>
                    </dd>
                </dl>

            </div>
            <div class="col-md-4">
                <!-- Info -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Comentarios</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="fb-comments" data-href="http://icerlly.com" data-numposts="5" data-width="100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Consejos</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <li>-Comprar dentro de la app, es comprar seguro</li>
                                    <li>-Acuerda puntos de encuentro en lugares públicos</li>
                                    <li>-Vende más rapido compartiendo en el <a href="https://www.facebook.com/groups/VENDEYGANA" target="_blank">Grupo</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!-- Info -->
            </div>
        </div>
    </div> <!-- /container -->

@endsection

@section('script_footer')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection