@extends('layouts.master')

@section('title', 'Publicar Trabajo')
@section('meta-description', 'Publicar Trabajo')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')
    <div class="container min-height" id="main">
        <div class="row">
            <div class="col-md-2">
            </div>
            @if(Auth::user()->isConfirmed())
            <div class="col-md-6">
                @if(Auth::user()->isAdmin() || Auth::user()->isStaff())
                <ul class="nav nav-pills nav-justified">
                    <li role="presentation"><a href="{{ route('new') }}">Publicar</a></li>
                    <li role="presentation"><a href="{{ route('republish') }}">Republicar</a></li>
                    <li role="presentation" class="active"><a href="{{ route('postJob') }}">Trabajo</a></li>
                </ul></br>
                @endif
                <h1>Publicar Trabajo</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }} <a href="{{ route('myVacancies') }}">ver mis vacantes</a>
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'jobStore', 'method'=>'POST', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                {!! Form::label('company', 'Empresa*') !!}
                {!! Form::text('company',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Empresa']) !!}
                {!! Form::label('title', 'Titulo*') !!}
                {!! Form::text('title',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Titulo de puesto']) !!}
                {!! Form::label('description', 'Descripción*') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Cuenta detalles de la vacante']) !!}
                {!! Form::label('city_name', 'Donde* (Nombre de la Ciudad) ') !!}
                {!! Form::text('city_name',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                {!! Form::label('telephone', 'Telefono*') !!}
                {!! Form::text('telephone',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Telefono']) !!}
                {!! Form::label('email', 'Email*') !!}
                {!! Form::text('email',null,['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Email']) !!}
                {!! Form::label('education', 'Educación*') !!}
                {!! Form::select('education',config('education.level') ,null,['class'=>'form-control']) !!}
                </br><button type="submit" class="btn btn-primary">Enviar</button>
            </div><!-- /fin de col -->

            <div class="col-md-4">
                <h2>Opcional</h2>
                {!! Form::label('salary', 'Salario') !!}
                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Salario (en pesos)</label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        {!! Form::number('salary',null,['class'=>'form-control', 'maxlength'=>'15', 'id'=>'exampleInputAmount','placeholder'=>'Salario (en pesos)']) !!}
                        <div class="input-group-addon">.00</div>
                    </div>
                </div> 
                {!! Form::label('lang', 'Idioma') !!}
                {!! Form::text('lang',null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Si aplica, escribe un segundo idioma']) !!}
                <h3>Atributos</h3>
                <p class="text-muted">Información relevante, ej. Habilidades, Experiencia en, Manejo de, etc.</p>
                <div id="addAttr" class="btn btn-info">Agregar Campo</div></br></br>
                <div class="form-inline">
                    <div id="ds-l"></div>
                    <div id="cp1">
                        <div class="form-group">
                            {!! Form::text('names[0]',null,['class'=>'form-control', 'id'=>'nm1','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[0]',null,['class'=>'form-control', 'id'=>'cn1','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp2">
                        <div class="form-group">
                            {!! Form::text('names[1]',null,['class'=>'form-control', 'id'=>'nm2','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[1]',null,['class'=>'form-control', 'id'=>'cn2','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp3">
                        <div class="form-group">
                            {!! Form::text('names[2]',null,['class'=>'form-control', 'id'=>'nm3','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[2]',null,['class'=>'form-control', 'id'=>'cn3','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp4">
                        <div class="form-group">
                            {!! Form::text('names[3]',null,['class'=>'form-control', 'id'=>'nm4','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[3]',null,['class'=>'form-control', 'id'=>'cn4','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp5">
                        <div class="form-group">
                            {!! Form::text('names[4]',null,['class'=>'form-control', 'id'=>'nm5','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[4]',null,['class'=>'form-control', 'id'=>'cn5','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp6">
                        <div class="form-group">
                            {!! Form::text('names[5]',null,['class'=>'form-control', 'id'=>'nm6','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[5]',null,['class'=>'form-control', 'id'=>'cn6','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp7">
                        <div class="form-group">
                            {!! Form::text('names[6]',null,['class'=>'form-control', 'id'=>'nm7','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[6]',null,['class'=>'form-control', 'id'=>'cn7','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                    <div id="cp8">
                        <div class="form-group">
                            {!! Form::text('names[7]',null,['class'=>'form-control', 'id'=>'nm8','maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('contents[7]',null,['class'=>'form-control', 'id'=>'cn8','maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                        </div>
                    </div>
                </div>  
                {!! Form::close() !!}
            </div><!-- /fin de col -->

            @else
                <div class="col-md-8">
                    <div class="alert alert-danger" role="alert">
                        <p>Tu email no se ha confirmado, te recomendamos revisar tu email (revisa también en la carpeta Spam) y confirmalo <a href="{{route('confirm_email')}}" class="alert-link">haciendo Click aquí</a></p>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            @endif
        </div><!-- /fin de row -->
    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min.js"></script>
<script src="{{ asset('assets/js/project.js') }}"></script>
<script src="{{ asset('assets/js/img-upl.js') }}"></script>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
@endsection




