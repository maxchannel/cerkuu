@extends('layouts.master')

@section('title', 'Buscar Anuncio')
@section('meta-description', 'Buscar Anuncio')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-md-1">
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
                    {!! Form::model(Request::only(['query','city']) ,['route'=>'searchAdvanced', 'method'=>'GET', 'class'=>'form-inline']) !!}
                        <div class="form-group">
                            <label for="query">Qué</label>
                            {!! Form::text('query',null,['class'=>'form-control', 'placeholder'=>'Producto o Servicio']) !!}
                        </div>
                        <div class="form-group ui-widget">
                            <label>Donde</label>
                            {!! Form::text('city',null,['class'=>'form-control', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Buscar</button>
                    {!! Form::close() !!}
                    </br></br>
                    <h1>Trabajos</h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">Anuncios más recientes</div>            
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Titulo</th>
                                </tr>
                            </thead>        

                            <tbody>
                                @foreach($jobs as $job)
                                <tr>
                                    <th>
                                        <script>
                                        moment.locale("es");
                                        document.writeln(moment.utc("{{ $job->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                        </script>
                                    </th>
                                    <td><a href="{{ route('job', [$job->id,$job->slug->content]) }}"><img src="../images/vendo.png" class="ad-image-min" alt="{{ $job->title }}"></a> <a href="{{ route('job', [$job->id,$job->slug->content]) }}">{{ $job->title }}</a></td>
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! str_replace('/?', '?', $jobs->appends(Request::only('query','city'))->render()) !!}

                </div><!--/.col-xs-6.col-lg-4-->

            </div><!--/row-->

        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 col-md-3 sidebar-offcanvas" id="sidebar">
            @include('partials.categories')
        </div><!--/.sidebar-offcanvas-->

    </div><!--/row-->
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/autocomplete.js') }}"></script>
@endsection


