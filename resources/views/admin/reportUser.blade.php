@extends('layouts.master')

@section('title', 'Reportar Usuario')
@section('meta-description', 'Reportar Usuario')

@section('content')
    <div class="container min-height">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <h1>Reportar Usuario</h1>
                <a href="{{ route('user', [$user->id]) }}">Regresar a Perfil</a>
                <div class="row">
                    <div class="col-md-8">
                        <p>{{ $user->name }}</p>
                        <img src="{{ ($user->avatar->route != 'default.jpg') ? asset('images/avatar/'.$user->avatar->route) : asset('images/default.jpg') }}" class="ad-image-min" alt="Responsive image">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                </br>
                @include('partials.errorMessages')
                <p>¿Motivo del reporte?*</p>
                {!! Form::open(['route'=>['reportUserStore', $user->id], 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::select('report',[
                ''=>'',
                '0'=>'Reportar Cuenta',
                '1'=>'Contenido Violento',
                '2'=>'Fraude',
                '4'=>'Productos Ilegales'] ,null,['class'=>'form-control']) !!}
                {!! Form::label('name', 'Nombre*') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
                {!! Form::label('email', 'Email*') !!}
                {!! Form::text('email',null,['class'=>'form-control', 'placeholder'=>'Tu Email']) !!}
                {!! Form::label('telephone', 'Teléfono (Opcional)') !!}
                {!! Form::text('telephone',null,['class'=>'form-control', 'placeholder'=>'Teléfono']) !!}
                {!! Form::label('comments', 'Comentarios(Opcional)') !!}
                {!! Form::textarea('comments',null,['class'=>'form-control', 'placeholder'=>'Escribe información detallada']) !!}
                </br><button type="submit" class="btn black">Reportar</button>

                {!! Form::close() !!}
                
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection