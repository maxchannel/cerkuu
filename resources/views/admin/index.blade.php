@extends('layouts.master')

@section('title', 'Usuarios Nuevos')
@section('meta-description', 'Usuarios Nuevos')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Usuarios Nuevos</h1>
                <ul class="list-inline">
                    <li>Usuarios: {{ $totalUsers }}</li>
                    <li>Anuncios: {{ $totalAds }}</li>
                    <li>Ventas: {{ $totalSales }}</li>
                    <li>Emails Enviados: {{ $totalEmails }}</li>
                    <li><a href="{{ route('reportedUsers') }}">Reportes</a></li>
                    <li><a href="{{ route('reportedUsers') }}">Emails</a></li>
                </ul>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation" class="active"><a href="{{ route('adminNewUsers') }}">Nuevos</a></li>
                    <li role="presentation"><a href="{{ route('adminNewAds') }}">Aprobar</a></li>
                    <li role="presentation"><a href="{{ route('adminNewEmails') }}">Emails</a></li>
                    <li role="presentation"><a href="{{ route('adminExpiredAds') }}">Expirados</a></li>
                    <li role="presentation"><a href="{{ route('adminRates') }}">Ratings</a></li>
                    <li role="presentation"><a href="{{ route('adminStatics') }}">Estadísticas</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Registro</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr data-id="{{ $user->id }}">
                            <th scope="row">{{ $user->id }}</th>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $user->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td><a href="{{ route('user',[$user->id]) }}" target="_blank">{{ ucfirst($user->name) }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if($user->confirmation()->exists())
                                <a href="#" class="btn-activate">Verificar</a>
                                @endif
                                <a href="#" class="btn-delete">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $users->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->

    {!! Form::open(['route'=>['deleteUser', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
    {!! Form::close() !!}

    {!! Form::open(['route'=>['activateUser', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-activate']) !!}
    {!! Form::close() !!}

@endsection

@section('script_footer')
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

    //Aprobar anuncio
    $('.btn-activate').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-activate');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });
});
</script>
@endsection
