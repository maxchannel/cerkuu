@extends('layouts.master')

@section('title', 'Emails Nuevos')
@section('meta-description', 'Emails Nuevos')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Emails sin enviar</h1>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('adminNewUsers') }}">Nuevos</a></li>
                    <li role="presentation"><a href="{{ route('adminNewAds') }}">Aprobar</a></li>
                    <li role="presentation" class="active"><a href="{{ route('adminNewEmails') }}">Emails</a></li>
                    <li role="presentation"><a href="{{ route('adminExpiredAds') }}">Expirados</a></li>
                    <li role="presentation"><a href="{{ route('adminRates') }}">Ratings</a></li>
                    <li role="presentation"><a href="{{ route('adminStatics') }}">Estadísticas</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Enviado</th>
                            <th>Message</th>
                            <th>Anuncio</th>
                            <th>From</th>
                            <th>Destiny</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($emails as $email)
                        <tr data-id="{{ $email->id }}">
                            <th scope="row">{{ $email->id }}</th>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $email->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>{{ $email->message }}</td>
                            <td><a href="{{ route('anuncio', [$email->ad->category->name, $email->ad->id, $email->ad->slug->content]) }}" target="_blank">{{ str_limit($email->ad->title, 30) }}</a></td>
                            <td>{{ $email->from }}</td>
                            <td>{{ $email->destiny }}</td>
                            <td>
                                <a href="#" class="btn-activate">Aprobar</a>
                                <a href="#" class="btn-delete">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $emails->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
    {!! Form::open(['route'=>['deleteEmail', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
    {!! Form::close() !!}

    {!! Form::open(['route'=>['activateEmail', ':USER_ID'], 'method'=>'POST', 'id'=>'form-activate']) !!}
    {!! Form::close() !!}
@endsection


@section('script_footer')
<script>
$(document).ready(function(){
    //Eliminar anuncio
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });
    //Aprobar anuncio
    $('.btn-activate').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-activate');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection
