@extends('layouts.master')

@section('title', 'Reportar Anuncio')
@section('meta-description', 'Reportar Anuncio')

@section('content')
    <div class="container min-height">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <h1>Reportar Anuncio</h1>
                <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">Regresar a Anuncio</a>
                <div class="row">
                    <div class="col-md-8">
                        <p>Anuncio:</p>
                        <img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="Responsive image"> {{ $ad->title }}
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                </br>
                @include('partials.errorMessages')
                <p>¿Motivo del reporte?*</p>
                {!! Form::open(['route'=>['reportAdStore', $ad->id], 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::select('report',[
                ''=>'',
                '0'=>'Producto vendido, Soy el dueño',
                '1'=>'Contenido Violento',
                '2'=>'Fraude',
                '3'=>'Duplicado',
                '4'=>'Producto Ilegal',
                '5'=>'Contenido inapropiado'] ,null,['class'=>'form-control']) !!}
                {!! Form::label('name', 'Nombre*') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre y Apellido']) !!}
                {!! Form::label('email', 'Email*') !!}
                {!! Form::text('email',null,['class'=>'form-control', 'placeholder'=>'Tu Email']) !!}
                {!! Form::label('telephone', 'Teléfono (Opcional)') !!}
                {!! Form::text('telephone',null,['class'=>'form-control', 'placeholder'=>'Teléfono']) !!}
                {!! Form::label('comments', 'Comentarios(Opcional)') !!}
                {!! Form::textarea('comments',null,['class'=>'form-control', 'placeholder'=>'Escribe información detallada']) !!}
                </br><button type="submit" class="btn black">Reportar</button>

                {!! Form::close() !!}
                
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection