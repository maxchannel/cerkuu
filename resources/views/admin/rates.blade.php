@extends('layouts.master')

@section('title', 'Ratings Nuevos')
@section('meta-description', 'Ratings Nuevos')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Usuarios Calificados</h1>
                <p class="text-muted">Usuarios recién calificados</p>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('adminNewUsers') }}">Nuevos</a></li>
                    <li role="presentation"><a href="{{ route('adminNewAds') }}">Aprobar</a></li>
                    <li role="presentation"><a href="{{ route('adminNewEmails') }}">Emails</a></li>
                    <li role="presentation"><a href="{{ route('adminExpiredAds') }}">Expirados</a></li>
                    <li role="presentation" class="active"><a href="{{ route('adminRates') }}">Ratings</a></li>
                    <li role="presentation"><a href="{{ route('adminStatics') }}">Estadísticas</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Estrellas</th>
                            <th>De</th>
                            <th>Calificando a</th>
                            <th>Comentarios</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rates as $rate)
                        <tr>
                            <th scope="row">{{ $rate->rate }}</th>
                            <td><a href="{{ route('user',[$rate->from_id]) }}" target="_blank">{{ $rate->from->name }}</a></td>
                            <td><a href="{{ route('user',[$rate->user_id]) }}" target="_blank">{{ $rate->user->name  }}</a></td>
                            <td>{{ $rate->comments }}</td>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $rate->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $rates->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection

@section('script_footer')
@endsection
