@extends('layouts.master')

@section('title', 'Scraping')
@section('meta-description', 'Scraping')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('scraper') }}" class="btn btn-primary">Regresar</a>
            </div>
            <div class="col-md-6">
                <h2>Scraping</h2>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'scrapingStore', 'method'=>'POST', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                    @for($i = 1; $i<=10; $i++)
                        <h3>Anuncio {{$i}}</h3>
                        {!! Form::label('title', 'Titulo '.$i) !!}
                        {!! Form::text('title'.$i,null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Titulo de tu anuncio']) !!}
                        {!! Form::label('description', 'Descripción '.$i) !!}
                        {!! Form::textarea('description'.$i, $description[$i],['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Cuenta detalles del producto que vendes']) !!}
                        {!! Form::label('price', 'Precio '.$i) !!}
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Precio (en pesos)</label>
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                {!! Form::number('price'.$i,null,['class'=>'form-control', 'id'=>'exampleInputAmount','placeholder'=>'Precio (en pesos)']) !!}
                                <div class="input-group-addon">.00</div>
                            </div>
                        </div> 
                        {!! Form::label('category_id', 'Categoría '.$i) !!}
                        {!! Form::select('category_id'.$i, config('options.categories') ,null,['class'=>'form-control']) !!}
                        {!! Form::label('city_name', 'Donde '.$i) !!}
                        {!! Form::text('city_name'.$i,'Guadalajara',['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                        {!! Form::label('link', 'Link '.$i) !!}
                        {!! Form::text('link'.$i,null,['class'=>'form-control', 'placeholder'=>'Link']) !!}
                        {!! Form::label('telephone', 'Telefono '.$i) !!}
                        {!! Form::text('telephone'.$i,null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Telefono']) !!}
                        {!! Form::label('email', 'Email '.$i) !!}
                        {!! Form::text('email'.$i,null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Email']) !!}
                        </br>{!! Form::file('file'.$i.'[]' ,['multiple' => 'true', 'accept' => 'image/*']) !!}
                        @if(Session::has('image-message'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('image-message') }}
                            </div>
                        @endif
                        <hr></br></br>
                    @endfor
                    </br><button type="submit" class="btn btn-primary">Enviar</button>
                {!! Form::close() !!}
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div> <!-- /container -->
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
@endsection
