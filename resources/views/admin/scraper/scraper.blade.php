@extends('layouts.master')

@section('title', 'Scraper')
@section('meta-description', 'Scraper')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h2>Scraper</h2>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @if(Session::has('image-message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('image-message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if(isset($_SESSION['facebook']))
                    {!! Form::open(['route'=>'scraping', 'method'=>'GET', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                    @for($i = 1; $i<=10; $i++)
                        {!! Form::label('id'.$i, 'ID '.$i) !!}
                        {!! Form::text('id'.$i,null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'ID '.$i]) !!}
                    @endfor
                    </br><button type="submit" class="btn btn-primary">Enviar</button>
                    {!! Form::close() !!}
                @endif
            </div>
            <div class="col-md-3">
                @if(!isset($_SESSION['facebook']))
                    <a href="{{ $helper->getLoginUrl($config['scopes']) }}" class="btn btn-primary">Iniciar sesión con Facebook!</a>
                @else
                    <p>
                        Bienvenido, {{ $facebook_user->getName() }}
                    </p>
                    <a href="{{ route('scraperLogout') }}" class="btn btn-danger">Cerrar sesión</a>
                @endif
            </div>
        </div>
    </div> <!-- /container -->
@endsection
