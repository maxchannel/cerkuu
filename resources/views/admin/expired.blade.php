@extends('layouts.master')

@section('title', 'Anuncios Expirados')
@section('meta-description', 'Anuncios Expirados')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Anuncios Expirados</h1>
                <p class="text-muted">Anuncios de los que ya pasaron 90 días</p>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('adminNewUsers') }}">Nuevos</a></li>
                    <li role="presentation"><a href="{{ route('adminNewAds') }}">Aprobar</a></li>
                    <li role="presentation"><a href="{{ route('adminNewEmails') }}">Emails</a></li>
                    <li role="presentation" class="active"><a href="{{ route('adminExpiredAds') }}">Expirados</a></li>
                    <li role="presentation"><a href="{{ route('adminRates') }}">Ratings</a></li>
                    <li role="presentation"><a href="{{ route('adminStatics') }}">Estadísticas</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Publicado</th>
                            <th>Usuario</th>
                            <th>Titulo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ads as $ad)
                        <tr data-id="{{ $ad->id }}">
                            <th scope="row">{{ $ad->id }}</th>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $ad->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td><a href="{{ route('user',[$ad->user->id]) }}" target="_blank">{{ ucfirst($ad->user->name) }}</a></td>
                            <td><a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}" target="_blank">{{ ucfirst($ad->title) }}</a></td>
                            <td>
                                <a href="#" class="btn-delete">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $ads->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->

    {!! Form::open(['route'=>['deleteExpired', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
    {!! Form::close() !!}

@endsection

@section('script_footer')
<script>
$(document).ready(function(){
    //Eliminar anuncio
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });
});
</script>
@endsection
