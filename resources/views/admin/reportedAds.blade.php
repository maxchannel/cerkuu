@extends('layouts.master')

@section('title', 'Anuncios Reportados')
@section('meta-description', 'Anuncios Reportados')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Anuncios Reportados</h1>
                <p class="text-muted">Anuncios reportados por usuarios <a href="{{ route('adminNewUsers') }}">Back</a></p>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('reportedUsers') }}">Usuarios</a></li>
                    <li role="presentation" class="active"><a href="{{ route('reportedAds') }}">Anuncios</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Publicado</th>
                            <th>Reportado</th>
                            <th>Email</th>
                            <th>Motivo</th>
                            <th>Titulo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reports as $report)
                        <tr data-id="{{ $report->ad->id }}" data-report="{{ $report->id }}">
                            <th scope="row">{{ $report->ad->id }}</th>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $report->ad->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $report->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>{{ $report->email }}</td>
                            <td>{{ $report->report }}</td>
                            <td><a href="{{ route('anuncio', [$report->ad->category->name, $report->ad->id, $report->ad->slug->content]) }}" target="_blank">{{ $report->ad->title }}</a></td>
                            <td>
                                <a href="#" class="btn-activate">Mantener</a>
                                <a href="#" class="btn-delete">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $reports->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
    {!! Form::open(['route'=>['deleteAd', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
    {!! Form::close() !!}

    {!! Form::open(['route'=>['unreportAd', ':USER_ID'], 'method'=>'POST', 'id'=>'form-activate']) !!}
    {!! Form::close() !!}
@endsection


@section('script_footer')
<script>
$(document).ready(function(){
    //Eliminar anuncio
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });
    //Aprobar anuncio
    $('.btn-activate').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('report');
        var form = $('#form-activate');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection
