@extends('layouts.master')

@section('title', 'Estadísticas')
@section('meta-description', 'Estadísticas')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Estadísticas</h1>
                <ul class="list-inline">
                    <li>Republish: {{ $republish }}</li>
                    <li>Usuario: {{ $user }}</li>
                    <li>Anónimo: {{ $anon }}</li>
                    <li>Hoy: {{ $today }}</li>
                    <li>Ayer: {{ $yesterday }}</li>
                    <li>Hora: {{ \Carbon::now('America/Mexico_City')}}</li>
                </ul>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('adminNewUsers') }}">Nuevos</a></li>
                    <li role="presentation"><a href="{{ route('adminNewAds') }}">Aprobar</a></li>
                    <li role="presentation"><a href="{{ route('adminNewEmails') }}">Emails</a></li>
                    <li role="presentation"><a href="{{ route('adminExpiredAds') }}">Expirados</a></li>
                    <li role="presentation"><a href="{{ route('adminRates') }}">Ratings</a></li>
                    <li role="presentation" class="active"><a href="{{ route('adminStatics') }}">Estadísticas</a></li>
                </ul></br>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Cantidad Total</th>
                            <th>Ayer</th>
                            <th>Hoy</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>staff@gmail.com</td>
                            <td>{{ $Staff1 }}</td>
                            <td>{{ $Staff1Yes }}</td>
                            <td>{{ $Staff1Today }}</td>
                        </tr>
                        <tr>
                            <td>staff@hotmail.com</td>
                            <td>{{ $Staff2 }}</td>
                            <td>{{ $Staff2Yes }}</td>
                            <td>{{ $Staff2Today }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection
