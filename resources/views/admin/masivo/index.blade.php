@extends('layouts.master')

@section('title', 'Masivo')
@section('meta-description', 'Masivo')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Masivo</h1>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if(isset($_SESSION['facebook']))
                {!! Form::open(['route'=>'masivoStore', 'method'=>'POST', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                    @for($i = 1; $i<=15; $i++)
                        <h3>Anuncio {{$i}}</h3>
                        {!! Form::label('title', 'Titulo '.$i) !!}
                        {!! Form::text('title'.$i,null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Titulo de tu anuncio']) !!}
                        {!! Form::label('description', 'Descripción '.$i) !!}
                        {!! Form::textarea('description'.$i, null,['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Cuenta detalles del producto que vendes']) !!}
                        {!! Form::label('price', 'Precio '.$i) !!}
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Precio (en pesos)</label>
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                {!! Form::number('price'.$i,null,['class'=>'form-control', 'id'=>'exampleInputAmount','placeholder'=>'Precio (en pesos)']) !!}
                                <div class="input-group-addon">.00</div>
                            </div>
                        </div> 
                        {!! Form::label('category_id', 'Categoría '.$i) !!}
                        {!! Form::select('category_id'.$i, config('options.categories') ,null,['class'=>'form-control']) !!}
                        {!! Form::label('city_name', 'Donde '.$i) !!}
                        {!! Form::text('city_name'.$i,'Guadalajara',['class'=>'form-control', 'maxlength'=>'70', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                        {!! Form::label('link', 'Link '.$i) !!}
                        {!! Form::text('link'.$i,null,['class'=>'form-control', 'placeholder'=>'Link']) !!}
                        {!! Form::label('telephone', 'Telefono '.$i) !!}
                        {!! Form::text('telephone'.$i,null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Telefono']) !!}
                        {!! Form::label('email', 'Email '.$i) !!}
                        {!! Form::text('email'.$i,null,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Email']) !!}
                        </br>{!! Form::file('file'.$i.'[]' ,['multiple' => 'true', 'accept' => 'image/*']) !!}
                        @if(Session::has('image-message'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('image-message') }}
                            </div>
                        @endif
                        <hr></br></br>
                    @endfor
                    </br><button type="submit" class="btn btn-primary">Enviar</button>
                {!! Form::close() !!}
                @endif
            </div>
            <div class="col-md-3">
                @if(!isset($_SESSION['facebook']))
                    <a href="{{ $helper->getLoginUrl($config['scopes']) }}" class="btn btn-primary">Iniciar sesión con Facebook!</a>
                @else
                    <p>
                        Bienvenido, {{ $facebook_user->getName() }}
                    </p>
                    <a href="{{ route('scraperLogout') }}" class="btn btn-danger">Cerrar sesión</a>
                @endif
            </div>
        </div>
    </div> <!-- /container -->
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
@endsection
