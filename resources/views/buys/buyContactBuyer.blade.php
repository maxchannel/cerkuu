@extends('layouts.master')

@section('title', 'Contactar al Comprador')
@section('meta-description', 'Contactar al Comprador')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Contactar con el Comprador</h1>
                <p class="text-info">Contactarás al comprador por el anuncio: {{ $sale->ad->title }}</p>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                <!-- contacto -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Enviar email</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                @include('partials.errorMessages')
                                {!! Form::open(['route'=>['buyContactBuyerSend',$sale->id], 'method'=>'POST', 'role'=>'form']) !!}
                                
                                <p>De ({{ $sale->seller->name }}): <strong>{{ $sale->seller->email }}</strong></p>
                                <p>Para ({{ $sale->buyer->name }}): <strong>{{ $sale->buyer->email }}</strong></p>
                                </br>
                                {!! Form::label('content', 'Mensaje*') !!}
                                {!! Form::textarea('content',null,['class'=>'form-control', 'placeholder'=>'Preguntas, mensajes o detalles para realizar tu venta']) !!}
                                </br><button type="submit" class="btn btn-success btn-lg">Enviar</button>
                                
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    @if($sale->buyer->telephone != "")
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Telefono del comprador</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <span class="glyphicon glyphicon-phone"></span> {{ $sale->buyer->telephone->number }}
                                @if($sale->buyer->telephone->whatsapp == 1)
                                <img src="../../images/whatsapp.png" class="whatsapp" data-toggle="tooltip" data-placement="top" title="WhatsApp en el Telefono">
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif

                </div>
                <!-- contacto -->
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection


@section('script_footer')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection