@extends('layouts.master')

@section('title', 'Comprar '.$ad->title)
@section('meta-description', 'Comprar '.$ad->title)

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                @include('partials.errorMessages')
                
                <div class="row">
                    @if(Auth::user()->isConfirmed())
                    <h1>Comprar: {{ $ad->title }}</h1>
                    <p class="text-muted">Al comprar podrás ver el telefono del vendedor y enviarle emails. El monto y la forma de pago se acuerda con el vendedor</p>
                    <p class="text-muted">Tendrás 10 días a partir de la compra para calificar la compra y al vendedor</p>
                    <p class="text-danger">Solo confirma la compra si realmente te interesa el producto, las cancelaciones pueden afectar tu ranking</p>
                    <img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min text-center" />
                    <dl class="dl-horizontal">
                        <dt>Precio</dt>
                        <dd class="ad-price">{{ number_format($ad->price) }} $</dd>
                        <dt>Vendedor</dt>
                        <dd><a href="{{ route('user',[$ad->user->id]) }}" target="_blank">{{ $ad->user->name }}</a></dd>
                        <dt>Rating</dt>
                        <dd>
                            @if($ad->user->rates()->count() > 0)
                                Puntaje: {{ round($ad->user->rates()->avg('rate')) }} ({{ $ad->user->rates->count() }} puntuaciones)
                            @else
                                Sin calificacines aún
                            @endif 
                        </dd>
                    </dl>
                </div>
                {!! Form::open(['route'=>['buyConfirm',$ad->id], 'method'=>'POST', 'role'=>'form']) !!}
                </br><button type="submit" class="btn btn-success btn-lg">Confirmar</button>
                {!! Form::close() !!}
                @else
                    <div class="alert alert-danger" role="alert">
                        <p>Tu email no se ha confirmado, te recomendamos revisar tu email (revisa también en la carpeta Spam) y confirmalo <a href="{{route('confirm_email')}}" class="alert-link">haciendo Click aquí</a></p>
                    </div>
                @endif
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection