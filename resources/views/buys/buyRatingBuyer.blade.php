@extends('layouts.master')

@section('title', 'Calificar Compra')
@section('meta-description', 'Calificar Compra')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container min-height">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <h1>Calificar al Comprador</h1>
                <div class="row">
                    <ul>
                        <li>Califica solo si la compra se concretó</li>
                        <li>Si han pasado más de 10 días y el comprador no responde puedes indicar en la calificación que el comprador canceló</li>
                    </ul>
                    <div class="col-md-8">
                        <p>Vendiste:</p>
                        <img src="{{ (count($sale->ad->images)) ? asset('images/ads/'.$sale->ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="Responsive image"> {{ $sale->ad->title }}
                    </div>
                    <div class="col-md-4">
                        Precio
                        <div class="ad-price">{{ $sale->ad->price }} $</div>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-md-8">
                        <p>Te compró: {{ $sale->buyer->name }}</p>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <p>¿Cómo calificas al comprador?</p>
                @include('partials.errorMessages')
                {!! Form::open(['route'=>['buyRatingBuyerSend', $sale->id], 'method'=>'POST', 'role'=>'form']) !!}

                {!! Form::select('rate',[
                ''=>'Calificación',
                '5'=>'5 estrellas',
                '4'=>'4 estrellas',
                '3'=>'3 estrellas',
                '2'=>'2 estrellas',
                '1'=>'1 estrella',
                '0'=>'El Comprador canceló'] ,null,['class'=>'form-control']) !!}
                </br>
                {!! Form::label('comments', 'Comentarios(Opcional)') !!}
                {!! Form::textarea('comments',null,['class'=>'form-control', 'placeholder'=>'Escribe los comentarios...']) !!}
                </br><button type="submit" class="btn black">Calificar</button>

                {!! Form::close() !!}
                
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection