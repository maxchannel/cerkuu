@extends('layouts.master')

@section('title', 'Busqueda Avanzada')
@section('meta-description', 'Busqueda Avanzada')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')

    <div class="container min-heightz">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Busqueda Avanzada <span class="glyphicon glyphicon-search font-1" aria-hidden="true"></span></h1>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'searchAdvanced', 'method'=>'GET', 'role'=>'form']) !!}
                {!! Form::label('query', 'Qué') !!}
                {!! Form::text('query',null,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Producto']) !!}
                </br>
                {!! Form::label('description', 'Descripción') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'maxlength'=>'1100','rows'=>'3', 'placeholder'=>'Palabras para buscar en la descripción del anuncio']) !!}
                </br>
                {!! Form::label('category_id', 'Categoría') !!}
                {!! Form::select('category_id',config('options.categories') ,null,['class'=>'form-control']) !!}
                </br>
                {!! Form::label('price', 'Precio') !!}
                <div class="form-inline">
                    <div class="form-group">
                        {!! Form::number('price_start',null,['class'=>'form-control','placeholder'=>'Desde']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::number('price_end',null,['class'=>'form-control','placeholder'=>'Hasta']) !!}
                    </div>
                </div>
                </br>
                {!! Form::label('city', 'Donde') !!}
                {!! Form::text('city',null,['class'=>'form-control', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}

                {!! Form::hidden('sort','date') !!}

                </br><button type="submit" class="btn btn-primary">Buscar</button>
                {!! Form::close() !!}

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
@endsection