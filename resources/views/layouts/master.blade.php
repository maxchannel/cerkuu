<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta-description')">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>@yield('title')</title>
    {!! HTML::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! HTML::style('assets/css/style.css') !!}
    {!! HTML::style('assets/css/offcanvas.css') !!}
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    @yield('script_head')
</head>
<body>
    @yield('script_body')
    <nav class="navbar navbar-fixed-top navbar-inverse">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}"><span class="glyphicon glyphicon-shopping-cart red" aria-hidden="true"></span> FijaAnuncios</a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                    @if(Auth::user()->isStaff())
                    <li><a href="{{ route('republish') }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Republicar</a></li>
                    <li><a href="{{ route('scraper') }}"><span class="glyphicon glyphicon-glass" aria-hidden="true"></span> Scraper</a></li>
                    <li><a href="{{ route('masivo') }}"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span> Masivo</a></li>
                    @else
                    <li><a href="{{ route('newAnon') }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Publicar</a></li>
                    @endif
                    <li><a href="{{ route('myAds') }}"><span class="glyphicon glyphicon-tags" aria-hidden="true"></span> Mis Anuncios</a></li>
                    <li>    

                        <a href="{{ route('notifications') }}"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> 
                            Notificaciones 
                            @if(Auth::user()->getNotifications() > 0)
                                <span class="badge red-n">{{ Auth::user()->getNotifications() }}</span>
                            @endif
                        </a>
                    </li>
                @endif
            </ul>

            <ul class="nav navbar-nav">
                <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <span class="icon icon-wh i-profile"></span> Explorar <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> Tecnología e Informática</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-scale" aria-hidden="true"></span> Coches y Motos</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-cd" aria-hidden="true"></span> Deporte y Ocio</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-lamp" aria-hidden="true"></span> Muebles, Deco y Jardin</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> Consolas y Videojuegos</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Libros, Películas y Música</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> Moda y Accesorios</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-ice-lolly-tasted" aria-hidden="true"></span> Juguetes, Niños y Bebés</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inmobiliaria</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-camera" aria-hidden="true"></span> Electrodomésticos</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Servicios</a></li>
                <li><a href="{{ route('anuncios') }}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Otros</a></li>
            </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Buscar">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    @if(Auth::check())
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="icon icon-wh i-profile"></span> {{ Auth::user()->name }}  <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('user', [Auth::user()->id]) }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Perfil</a></li>
                        <li><a href="{{ route('mySales') }}"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Mis Ventas</a></li>
                        <li><a href="{{ route('myPurchases') }}"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mis Compras</a></li>
                        <li><a href="{{ route('settings_profile') }}"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Configuración</a></li>
                        <li><a href="{{ route('logout') }}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Salir</a></li>
                    </ul>
                    @else
                    <li><a href="{{ route('newAnon') }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Publicar</a></li>
                    <form class="navbar-form navbar-right">

                        <a href="{{ route('login') }}" class="btn btn-primary"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a>
                    </form>
                    @endif
                </li>
            </ul>

          </div><!-- /.nav-collapse -->
        </div><!-- /.container -->
    </nav><!-- /.navbar -->

    @yield('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    @yield('script_footer')
</body>
</html>
