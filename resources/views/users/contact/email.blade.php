@extends('layouts.master')

@section('title', 'Contactar al Vendedor')
@section('meta-description', 'Contactar al Vendedor')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Contactar al Vendedor</h1>
                <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">Regresar a Anuncio</a>
                <p>Email para: {{ str_limit($ad->email->content, 3) }}</p>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Email Enviado <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">regresar a anuncio</a>
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>['anuncioEmailingStore', $ad->id], 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::label('title', 'Nombre Completo*') !!}
                {!! Form::text('title',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Nombre y Apellidos']) !!}
                {!! Form::label('email', 'Email*') !!}
                {!! Form::text('email',(\Auth::check()) ? \Auth::user()->email : null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Email']) !!}
                {!! Form::label('message', 'Mensaje*') !!}
                {!! Form::textarea('message',null,['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Mensaje al vendedor']) !!}

                </br><button type="submit" class="btn btn-primary">Enviar</button>
                {!! Form::close() !!}
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endsection