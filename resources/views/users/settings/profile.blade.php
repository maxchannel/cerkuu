@extends('layouts.master')

@section('title', 'Configuración')
@section('meta-description', 'Configuración')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Configuración de Perfil</h1>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation" class="active"><a href="{{ route('settings_profile') }}">Perfil</a></li>
                    <li role="presentation"><a href="{{ route('settings_avatar') }}">Avatar</a></li>
                    <li role="presentation"><a href="{{ route('settings_email') }}">Email</a></li>
                </ul></br>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::model($user, ['route'=>'settings_profile_update', 'method'=>'PUT', 'role'=>'form']) !!}

                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Nombre para mostrar']) !!}

                {!! Form::label('username', 'Username') !!}
                {!! Form::text('username',null,['class'=>'form-control', 'maxlength'=>'16', 'placeholder'=>'Username']) !!}
                
                {!! Form::label('number', 'Teléfono (Campo vacio no se muestra)') !!}
                {!! Form::text('number',$user->telephone->number,['class'=>'form-control', 'maxlength'=>'15', 'placeholder'=>'Teléfono (Recomendado)']) !!}
                <div class="checkbox">
                    <label class="remember">
                        @if($user->telephone->whatsapp == 1)
                            {!! Form::checkbox('whatsapp','yes',true) !!} WhatsApp en el telefono
                        @else
                            {!! Form::checkbox('whatsapp','yes',false) !!} WhatsApp en el telefono
                        @endif
                    </label>
                </div>  
                <p class="text-muted">Teléfono que se mostrará para contactar a tus compradores y vendedores.</p>

                </br><button type="submit" class="btn btn-success">Actualizar</button>
                {!! Form::close() !!}

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop