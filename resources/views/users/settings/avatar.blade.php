@extends('layouts.master')

@section('title', 'Configuración')
@section('meta-description', 'Configuración')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Configuración de Avatar</h1>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('settings_profile') }}">Perfil</a></li>
                    <li role="presentation" class="active"><a href="{{ route('settings_avatar') }}">Avatar</a></li>
                    <li role="presentation"><a href="{{ route('settings_email') }}">Email</a></li>
                </ul></br>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                <label>Actual:</label>

                <img src="{{ ($user->avatar->route != 'default.jpg') ? asset('images/avatar/'.$user->avatar->route) : asset('images/default.jpg') }}" class="profile-avatar-settings" /></br></br>

                {!! Form::open(['route'=>'settings_avatar_update', 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                {!! Form::file('file', ['accept' => 'image/*']) !!}
                </br><button type="submit" class="btn btn-primary">Actualizar</button>
                {!! Form::close() !!}

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop