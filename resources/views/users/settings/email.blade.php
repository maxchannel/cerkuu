@extends('layouts.master')

@section('title', 'Configuración')
@section('meta-description', 'Configuración')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Configuración de Email</h1>
                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('settings_profile') }}">Perfil</a></li>
                    <li role="presentation"><a href="{{ route('settings_avatar') }}">Avatar</a></li>
                    <li role="presentation" class="active"><a href="{{ route('settings_email') }}">Email</a></li>
                </ul></br>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                <p class="text-warning">El nuevo email debe ser confirmado</p>
                <label>Actual:</label>
                <p>{{$user->email}}</p>
                @if(!Auth::user()->isConfirmed())
                    <div class="alert alert-danger" role="alert">
                        <p>Tu email no se ha confirmado, te recomendamos revisar tu email (revisa también en la carpeta Spam) y confirmalo <a href="{{route('confirm_email')}}" class="alert-link">Haciendo Click aquí</a></p>
                    </div>
                @endif

                {!! Form::open(['route'=>'settings_email_update', 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                {!! Form::label('email', 'Nuevo Email') !!}
                {!! Form::email('email',null,['class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Email válido']) !!}
                <div class="checkbox">
                    <label class="remember">
                        {!! Form::checkbox('promo','yes',true) !!} Promociones
                    </label>
                </div>
                <p class="text-muted">Recibir emails promocionales</p>
                </br><button type="submit" class="btn btn-primary">Actualizar</button>
                {!! Form::close() !!}


           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop