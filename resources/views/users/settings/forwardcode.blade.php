@extends('layouts.master')

@section('title', 'Reenviar Código a Email')
@section('meta-description', 'Reenviar Código a Email')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Reenviar Código a Email</h1>

                @if (Session::has('deleteError'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('deleteError') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if(!Auth::user()->isConfirmed())
                    {!! Form::open(['route'=>'forwardcode_send', 'method'=>'PUT', 'role'=>'form']) !!}
                    <p>El código será enviado a: {{ Auth::user()->email }}</p>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    {!! Form::close() !!}
                @else
                    <div class="alert alert-success" role="alert">Tu cuenta fue confirmada</div>
                    <a href="{{ route('settings_email') }}" class="btn btn-primary">Volver</a>
                @endif
                

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop