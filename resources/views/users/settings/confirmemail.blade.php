@extends('layouts.master')

@section('title', 'Confirmar Email')
@section('meta-description', 'Confirmar Email')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Confirmación de Email</h1>

                @if (Session::has('deleteError'))
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('deleteError') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if(!Auth::user()->isConfirmed())
                    {!! Form::open(['route'=>'confirm_email_update', 'method'=>'POST', 'role'=>'form']) !!}
                    {!! Form::label('key', 'Introduce el Código enviado a tu email') !!}
                    {!! Form::text('key',null,['class'=>'form-control', 'placeholder'=>'Código']) !!}
                    </br><button type="submit" class="btn btn-primary">Confirmar</button>
                    {!! Form::close() !!}
                </br><p class="text-muted">¿No recibiste el código? <a href="{{ route('forwardcode') }}">Reenviar</a></p>
                @else
                    <div class="alert alert-success" role="alert">Tu cuenta fue confirmada</div>
                    <a href="{{ route('settings_email') }}" class="btn btn-primary">Volver</a>
                @endif
                

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop