@extends('layouts.master')

@section('title', 'Registro')
@section('meta-description', 'Registro')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Registro</h1>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'signup', 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Nombre completo']) !!}

                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email',null,['class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Email válido']) !!}

                {!! Form::label('username', 'Username') !!}
                {!! Form::text('username',null,['class'=>'form-control', 'maxlength'=>'16', 'placeholder'=>'Username']) !!}

                {!! Form::label('password', 'Contraseña') !!}
                {!! Form::password('password',['class'=>'form-control', 'maxlength'=>'25']) !!}
                </br>
                Al enviar aceptas la <a href="{{route('privacy')}}" target="_blank">Política de Privacidad</a> y los <a href="{{route('terms')}}" target="_blank">Términos y condiciones</a>
                </br></br><button type="submit" class="btn btn-success">Registrarme</button>
                {!! Form::close() !!}

            </br><p class="text-muted">Ya tiene cuenta? <a href="{{ route('login') }}">Inicia Sesión</a></p>

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@stop