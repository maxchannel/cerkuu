@extends('layouts.master')

@section('title'){{ $user->name }}@endsection
@section('meta-description'){{ $user->name }}@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="thumbnail">
                  <img src="{{ ($user->avatar->route != 'default.jpg') ? asset('images/avatar/'.$user->avatar->route) : asset('images/default.jpg') }}" alt="Chania" class="img-responsive">
                  <div class="caption">
                        <h3>{{ $user->name }} ({{ '@'.$user->username}})</h3>
                </div>
                <dl class="dl-horizontal">
                    <dt>Calificación:</dt>
                    <dd>
                        @if($user->rates()->count() > 0)
                            Puntaje: {{ round($user->rates()->avg('rate')) }} ({{ $user->rates->count() }} puntuaciones)
                        @else
                            Sin calificaciones aún
                        @endif 
                    </dd>
                    <dt>Ventas:</dt>
                    <dd>{{ $sales->count() }}</dd>
                    <dt>Compras:</dt>
                    <dd>{{ $purchases->count() }}</dd>
                    <dt>Cancelaciones:</dt>
                    <dd>{{ $cancelations }}</dd>
                </dl>
                <a href="{{ route('reportUser', [$user->id]) }}">Reportar</a>
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->

@endsection