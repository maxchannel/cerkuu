@extends('layouts.master')

@section('title', 'Notificaciones')
@section('meta-description', 'Notificaciones')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Notificaciones</h1>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notifications as $notification)
                            @if($notification->type == 'bought')
                                <tr>
                                    <td>
                                        Te compraron {{ $notification->ad->title }}
                                        <script>
                                        moment.locale("es");
                                        document.writeln(moment.utc("{{ $notification->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                        </script>
                                    </td>
                                    <td><a href="{{ route('mySales') }}" class="btn btn-primary btn-xs">Ver Ventas</a></td>
                                </tr>
                            @elseif($notification->type == 'welcome')
                                <tr>
                                    <td>
                                        Bienvenido a FijaAnuncios! :)
                                        <script>
                                        moment.locale("es");
                                        document.writeln(moment.utc("{{ $notification->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                        </script>
                                    </td>
                                    <td></td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $notifications->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection