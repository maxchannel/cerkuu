@extends('layouts.master')

@section('title', 'Mensajes')
@section('meta-description', 'Mensajes')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Mensajes</h1>

                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif


            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection