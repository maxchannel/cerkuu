@extends('layouts.master')

@section('title', trans('categories.'.$name))
@section('meta-description', trans('categories.'.$name))

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-md-1">
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
                    {!! Form::model(Request::only(['query','city']) ,['route'=>'searchAdvanced', 'method'=>'GET', 'class'=>'form-inline']) !!}
                        <div class="form-group">
                            <label for="query">Qué</label>
                            {!! Form::text('query',null,['class'=>'form-control', 'placeholder'=>'Producto o Servicio']) !!}
                        </div>
                        <div class="form-group ui-widget">
                            <label>Donde</label>
                            {!! Form::text('city',null,['class'=>'form-control', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Buscar</button>
                    {!! Form::close() !!}
                    </br>
                    @include('partials.ads')
                    <h1>{{ trans('categories.'.$name) }}</h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            @if($ads->count() > 0)
                            Listado de Anuncios

                            <!-- <p class="pull-right">
                                Ordenar(
                                    @if(\Request::input('sort') == 'date' || \Request::input('sort') == 'date_a')
                                    Fecha
                                    @else
                                    <a href="{{ route('category', ['sort'=>'date', 'name'=>$name]) }}">Fecha</a>
                                    @endif
                                    /
                                    @if(\Request::input('sort') == 'price' || \Request::input('sort') == 'price_a')
                                    Precio
                                    @else
                                    <a href="{{ route('category', ['sort'=>'price', 'name'=>$name]) }}">Precio</a>
                                    @endif
                                    )

                                    @if(\Request::input('sort') == 'price')
                                    <a href="{{ route('category', ['sort'=>'price_a', 'name'=>$name]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                                    @elseif(\Request::input('sort') == 'price_a')
                                    <a href="{{ route('category', ['sort'=>'price', 'name'=>$name]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                                    @endif

                                    @if(\Request::input('sort') == 'date')
                                    <a href="{{ route('category', ['sort'=>'date_a', 'name'=>$name]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                                    @elseif(\Request::input('sort') == 'date_a')
                                    <a href="{{ route('category', ['sort'=>'date', 'name'=>$name]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                                    @endif
                            </p> -->

                            @else
                            <h3>Sin Resultados</h3>
                            @endif
                        </div>              
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Titulo</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>        

                            <tbody>
                                @foreach($ads as $ad)
                                <tr>
                                    <th>
                                        <script>
                                        moment.locale("es");
                                        document.writeln(moment.utc("{{ $ad->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                        </script>
                                    </th>
                                    <td><a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}"><img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="{{ $ad->title }}"></a> <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">{{ ucfirst($ad->title) }}</a></td>
                                    <td>
                                        @if($ad->price > 0)
                                        {{ number_format($ad->price) }} $
                                        @else
                                        Sin precio
                                        @endif
                                    </td>
                                </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @include('partials.ads')
                    {!! str_replace('/?', '?', $ads->appends(Request::only('query','city'))->render()) !!}

                </div><!--/.col-xs-6.col-lg-4-->

            </div><!--/row-->

        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 col-md-3 sidebar-offcanvas" id="sidebar">
            @include('partials.categories')
            @include('partials.ads')
        </div><!--/.sidebar-offcanvas-->

    </div><!--/row-->
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/autocomplete.js') }}"></script>
@endsection


