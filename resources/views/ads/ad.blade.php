@extends('layouts.master')

@section('title'){{ $ad->title }}@endsection
@if($ad->price > 0)
    @section('meta-description'){{ number_format($ad->price).'$ - '.$ad->description }}@endsection
@else
    @section('meta-description'){{ $ad->description }}@endsection
@endif

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('script_body')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection

@section('content')
    <div class="container min-height">
        <div class="row">
            <div class="col-md-7">
                
                @include('partials.ads')
                <!-- Imágen -->
                @if(count($ad->images))
                <div class="bs-example" data-example-id="simple-carousel">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach($ad->images as $key => $image)
                                    @if($key == 0)
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                                    @else
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                                    @endif
                                @endforeach  
                            </ol>

                            <div class="carousel-inner" role="listbox">
                                @foreach($ad->images as $key => $image)
                                    @if($key == 0)
                                        <div class="item active">
                                            <img src="../../../images/ads/{{ $image->route }}" class="tales" alt="{{ $ad->title }}">
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="../../../images/ads/{{ $image->route }}" class="tales" alt="{{ $ad->title }}">
                                        </div> 
                                    @endif
                                @endforeach  
                            </div>

                            @if(count($ad->images) > 1)
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            @endif
                      </div>
                </div>
                @endif
                <!-- Imágen -->
                </br>
                @if(count($ad->images))
                    @include('partials.ads')
                @endif

            </div>
            <div class="col-md-5">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default panel-contact">
                        <!-- Info -->
                        <ul class="list-unstyled">
                            <li class="panel-contact-price font-2">
                                @if($ad->price != "" && $ad->price != 0)
                                    {{ number_format($ad->price) }}$
                                @endif
                            </li>
                            <li class="word-wrap">
                                <h1 class="panel-contact-title">{{ ucfirst($ad->title) }}</h1>
                            </li>
                            <li class="panel-contact-description">
                                {!! nl2br(e(ucfirst($ad->description))) !!}
                            </li>
                            <li class="panel-contact-category">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>$ad->category->id]) }}">{{ trans('categories.'.$ad->category->name) }}</a>
                            </li>
                            <li>
                                <span class="glyphicon glyphicon-map-marker"></span><a href="{{ route('searchAdvanced', ['city'=>$ad->city_name]) }}"> {{ ucfirst($ad->city_name) }}</a>
                            </li>
                            <li class="panel-contact-map">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.google.com/maps?q={{$ad->city_name}}&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </li>
                            

                            <li>
                                <div class="fb-share-button" data-href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}" data-layout="button"></div>
                                <div class="fb-send" data-href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}"></div>
                            </li>
                            <li>
                                @if($ad->role == 'anon')
                                <a href="{{ route('preEditAnonAd', [$ad->id]) }}" class="text-info">Editar o Eliminar</a>
                                @endif
                            </li>
                        </ul>
                        <!-- Info -->
                    </div>

                    <div class="panel panel-default panel-contact">
                        <ul class="list-unstyled">
                            @if($ad->role == 'anon' || $ad->role == 'anonuser')
                                {{ ucfirst($ad->name->content) }}
                            @elseif($ad->role == 'user')
                                <a href="{{ route('user',[$ad->user->id]) }}">{{ $ad->user->name }}</a> 
                                @if($ad->user->rates->count() > 0)
                                    {{ round($ad->user->rates()->avg('rate')) }} ({{ $ad->user->rates->count() }} puntuaciones)
                                @else
                                    (Sin Ranking)
                                @endif
                            @endif
                            <li>
                                @if($ad->role == 'republish')
                                    @if($ad->telephone()->exists())
                                    <button id="show" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-phone"></span> Teléfono</button> <span id="telephone1">{{ $ad->telephone->content }}</span>
                                    @endif
                                @elseif($ad->role == 'anon' || $ad->role == 'anonuser')
                                    @if($ad->telephone()->exists())
                                    <dd><button id="show" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-phone"></span> Teléfono</button> <span id="telephone1">{{ $ad->telephone->content }}</span></dd>
                                    @endif
                                @elseif($ad->role == 'user')
                                    @if($ad->user->telephone->number != "")
                                        <button id="show" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-phone"></span> Teléfono</button> <span id="telephone1">{{ $ad->user->telephone->number }}</span>
                                        @if($ad->user->telephone->whatsapp == 1)
                                        <img src="../../../images/whatsapp.png" class="whatsapp" data-toggle="tooltip" data-placement="top" title="Whatsapp disponible" />
                                        @endif
                                    @endif
                                @endif 
                            </li> 
                            <li>
                                @if($ad->role == 'anon' || $ad->role == 'anonuser')
                                    @if(Auth::check())
                                        @if($ad->user->id == Auth::user()->id)
                                            <a href="{{ route('editAd', [$ad->id]) }}" type="button" class="btn btn-warning btn-sm">Editar</a> <a href="{{ route('editAd', [$ad->id]) }}" type="button" class="btn btn-danger">Eliminar</a>
                                        @else
                                            <a href="{{ route('buy', [$ad->id]) }}" type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Comprar</a>
                                        @endif
                                    @else
                                        <a href="{{ route('anuncioEmailing', [$ad->id]) }}" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Enviar Email</a>
                                    @endif
                                @elseif($ad->role == 'republish')
                                    @if($ad->link()->exists())
                                    <a href="{{ $ad->link->content }}" type="button" class="btn btn-success btn-sm" target="_blank"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Contactar al Vendedor</a>
                                    @endif
                                    @if($ad->email()->exists())
                                    <a href="{{ route('anuncioEmailing', [$ad->id]) }}" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Enviar Email</a>
                                    @endif
                                @elseif($ad->role == 'user')
                                    @if(Auth::check())
                                        @if($ad->user->id == Auth::user()->id)
                                            <a href="{{ route('editAd', [$ad->id]) }}" type="button" class="btn btn-warning btn-sm">Editar</a> <a href="{{ route('editAd', [$ad->id]) }}" type="button" class="btn btn-danger">Eliminar</a>
                                        @else
                                            <a href="{{ route('buy', [$ad->id]) }}" type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Comprar</a>
                                        @endif
                                    @else
                                        <a href="{{ route('login') }}" type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Comprar</a>
                                    @endif
                                @endif
                            </li>
                        </ul>
                    </div>
                  
                </div><!-- Info -->

                <div class="hidden-xs hidden-md">
                @include('partials.ads')
                </div>
            </div>
        </div>
    </div> <!-- /container -->
@endsection

@section('script_footer')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function(){
    $("#telephone1").hide();
    $("#show").click(function(){
        $("#telephone1").show();
    });
});
</script>
@endsection