@extends('layouts.master')

@section('title', 'Editar Anuncio')
@section('meta-description', 'Editar Anuncio')

@section('content')
@if($ad->role == 'anon')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Editar Anuncio</h1>
                <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">Regresar a Anuncio</a>
                <p>Titulo: {{ $ad->title }}</p>
                @if (Session::has('message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>['preEditAnonAdSend', $ad->id], 'method'=>'POST', 'role'=>'form']) !!}

                {!! Form::label('password', 'Contraseña') !!}
                {!! Form::password('password',['class'=>'form-control', 'maxlength'=>'25']) !!}

                </br><button type="submit" class="btn btn-success">Editar</button>
                {!! Form::close() !!}

           </div>
            <div class="col-md-3">
            </div>
        </div>
    </div> <!-- /container -->
@endif
@stop