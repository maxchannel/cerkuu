@extends('layouts.master')

@section('title', 'Editar Anuncio')
@section('meta-description', 'Editar Anuncio')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')
@if($ad->key->content == $key)
    <div class="container min-height">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <h1>Editar Anuncio</h1>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::model($ad,['route'=>['editAnonAdStore', $ad->id, $key], 'method'=>'PUT', 'role'=>'form']) !!}
                {!! Form::label('title', 'Titulo*') !!}
                {!! Form::text('title',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Titulo de tu anuncio']) !!}
                {!! Form::label('description', 'Descripción*') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'maxlength'=>'1100', 'placeholder'=>'Cuenta detalles del producto que vendes']) !!}
                {!! Form::label('price', 'Precio*') !!}
                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Precio (en pesos)</label>
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        {!! Form::number('price',null,['class'=>'form-control', 'maxlength'=>'15', 'id'=>'exampleInputAmount','placeholder'=>'Precio (en pesos)']) !!}
                        <div class="input-group-addon">.00</div>
                    </div>
                </div>
                {!! Form::label('city_name', 'Donde* (Escribe el nombre de tu ciudad) ') !!}
                {!! Form::text('city_name',null,['class'=>'form-control', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                {!! Form::label('name', 'Nombre*') !!}
                {!! Form::text('name',$ad->name->content,['class'=>'form-control', 'placeholder'=>'Nombre que aparecerá en el anuncio']) !!}
                @if($ad->telephone()->exists())
                {!! Form::label('telephone', 'Telephone') !!}
                {!! Form::text('telephone',$ad->telephone->content,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Telephono']) !!}
                @endif
                </br><button type="submit" class="btn btn-primary">Actualizar</button>
                <a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}" class="btn btn-warning">Salir</a>
            </div>
            <div class="col-md-4">
                <h2>Opcional</h2>
                @if($ad->delivery()->exists())
                {!! Form::label('delivery', 'Entregas (Opcional)') !!}
                {!! Form::text('delivery',$ad->delivery->place,['class'=>'form-control', 'maxlength'=>'70', 'placeholder'=>'Si aplica, escribe los lugares donde entregas']) !!}
                @endif

                <h3>Imágenes</h3>
                <img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="{{ $ad->title }}">
                <a href="{{ route('editAnonImageAd', [$ad->id, $key]) }}" class="btn btn-success"><span class="glyphicon glyphicon-picture"></span> Editar Imágenes</a>

                @if($ad->attributes()->exists())
                <h3>Atributos</h3>
                <p class="text-muted">Información relevante, ej. Marca, Año, Modelo, etc.</p>
                <div class="form-inline">
                    
                        @foreach($ad->attributes as $llave=>$attribute)
                            <div>
                                <div class="form-group">
                                    {!! Form::text('names['.$llave.']',$attribute->name,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Nombre']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::text('contents['.$llave.']',$attribute->content,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Contenido']) !!}
                                    {!! Form::hidden('atrs_id['.$llave.']', $attribute->id) !!}
                                </div>
                            </div>
                        @endforeach
                </div>
                @endif
            {!! Form::close() !!}

            {!! Form::open(['route'=>['deleteAdAnon', $ad->id, $key], 'method'=>'DELETE']) !!}
            </br><button type="submit" class="btn btn-danger" onclick="return confirm('¿Confirma que realmente quiere ELIMINAR EL ANUNCIO?')" ><span class="glyphicon glyphicon-trash"></span> Eliminar Anuncio</button>
            {!! Form::close() !!}
            </div>
        </div>
    </div> <!-- /container -->
@endif
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ asset('assets/js/autocomplete.js') }}"></script>
<script src="{{ asset('assets/js/project.js') }}"></script>
@endsection

