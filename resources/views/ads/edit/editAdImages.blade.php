@extends('layouts.master')

@section('title', 'Editar Anuncio')
@section('meta-description', 'Editar Anuncio')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-7">
                <h1>Editar Imágenes de {{ $ad->title }}</h1>
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($images->count() > 0)
                    @foreach($images as $image)
                        <div class="col-sm-2">
                            <img src="../../images/ads/{{ $image->route }}" class="ad-image-min" />
                            {!! Form::open(['route'=>['editAdImageDestroy', $image->id], 'method'=>'DELETE']) !!}
                            {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm btn-block', 'style' => 'margin: 10px 0']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Anuncio aún no tiene imágenes.
                    </div>
                @endif

            </div>
            <div class="col-md-3">
                <h2>Información</h2>
                <a href="{{ route('editAd', [$ad->id]) }}" class="btn btn-success">Volver a editar Información</a>

                <!-- images -->
                {!! Form::open(['route'=>['editAdImageUpdate', $ad->id], 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <h3>Subir más Imágenes</h3>
                <p class="text-muted">Puedes subir multiples imágenes a tu anuncio</p>
                @if(Session::has('image-message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('image-message') }}
                    </div>
                @endif
                <div class="form-group">
                    <div id="browse" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-picture"></span> Seleccionar imágenes</div>
                </div>
                <button type="submit" class="btn btn-primary">Subir</button>
                {!! Form::file('file[]',['multiple' => 'multiple', 'id' => 'multiple-files', 'accept' => 'image/*']) !!}
                <div class="form-group" id="form-buttons">            -
                    </br>{!! Form::reset('Cancelar', array('class' => 'btn btn-warning btn-block', 'id' => 'reset')) !!}
                </div>
                <div id="files"></div>

                {!! Form::close() !!}
                <!-- images -->
            </div>
        </div>
    </div> <!-- /container -->

@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/img-upl.js') }}"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Confirmas eliminar la imágen?")){
          return false;
        }
    });
</script>
@endsection

