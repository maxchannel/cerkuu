@extends('layouts.master')

@section('title', 'Mis Ventas')
@section('meta-description', 'Mis Ventas')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Mis Ventas</h1>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Vendido</th>
                            <th></th>
                            <th>Anuncio</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sales as $sale)
                        <tr data-id="{{ $sale->ad->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $sale->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td><img src="{{ (count($sale->ad->images)) ? asset('images/ads/'.$sale->ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="Responsive image"></td>
                            <td>{{ $sale->ad->title }}</td>
                            <td>
                                <a href="{{ route('buyContactBuyer', [$sale->id]) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Contactar al Comprador</a></br></br>
                                <a href="{{ route('buyRatingBuyer', [$sale->id]) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Calificar al Comprador</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $sales->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection