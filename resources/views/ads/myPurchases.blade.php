@extends('layouts.master')

@section('title', 'Mis Compras')
@section('meta-description', 'Mis Compras')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Mis Compras</h1>

                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Comprado</th>
                            <th></th>
                            <th>Titulo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($purchases as $purchase)
                        <tr data-id="{{ $purchase->ad->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $purchase->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td><img src="{{ (count($purchase->ad->images)) ? asset('images/ads/'.$purchase->ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="Responsive image"></td>
                            <td>{{ $purchase->ad->title }}</td>
                            <td>
                                <a href="{{ route('buyContact', [$purchase->id]) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Contactar al Vendedor</a></br></br>
                                <a href="{{ route('buyRatingSeller', [$purchase->id]) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Calificar al Vendedor</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $purchases->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection