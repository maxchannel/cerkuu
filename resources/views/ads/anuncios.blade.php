@extends('layouts.master')

@section('title', 'Anuncios Clasificados')
@section('meta-description', 'Anuncios Clasificados')

@section('script_head')
{!! HTML::style('assets/css/jquery-ui.css') !!}
@endsection

@section('content')
    <div class="container min-height margin-bottom">
        <div class="row">
            @include('partials.ads')
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-8">
                        <div class="jumbotron ad-banner" id="landing">
                            <p><a class="btn btn-default btn-lg" href="{{ route('searchAdvanced', ['city'=>'guadalajara']) }}" role="button">Anuncios en Guadalajara</a></p>
                        </div>
                        <div class="jumbotron ad-banner" id="iphone">
                            <p><a class="btn btn-default btn-lg" href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>1]) }}" role="button">Smarthphones</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 hidden-xs">
                                    <div class="jumbotron ad-banner" id="tlaquepaque">
                                <p><a class="btn btn-default" href="{{ route('searchAdvanced', ['city'=>'tlaquepaque']) }}" role="button">Anuncios en Tlaquepaque</a></p>
                            </div>
                            </div>
                            <div class="col-md-12 hidden-xs">
                                    <div class="jumbotron ad-banner" id="zapopan">
                                <p><a class="btn btn-default" href="{{ route('searchAdvanced', ['city'=>'zapopan']) }}" role="button">Anuncios en Zapopan</a></p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::model(Request::only(['query','city']) ,['route'=>'searchAdvanced', 'method'=>'GET', 'class'=>'form-inline']) !!}
                    <div class="form-group">
                        <label for="query">Qué</label>
                        {!! Form::text('query',null,['class'=>'form-control', 'placeholder'=>'Producto o Servicio']) !!}
                    </div>
                    <div class="form-group ui-widget">
                        <label>Donde</label>
                        {!! Form::text('city',null,['class'=>'form-control', 'id'=>'city', 'placeholder'=>'Ciudad']) !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Buscar</button>
                {!! Form::close() !!}
                </br></br>

                @include('partials.ads')

                <div class="row">
                    <div class="col-md-3">
                        <!-- hogar -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Hogar</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>12]) }}" class="list-group-item">Electrodomésticos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>13]) }}" class="list-group-item">Muebles</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>7]) }}" class="list-group-item">Electrónicos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>21]) }}" class="list-group-item">Pantallas</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>15]) }}" class="list-group-item">Servicios</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>16]) }}" class="list-group-item">Accesorios</a>
                            </div>
                        </div>
                        <!-- hogar -->
                    </div>
                    <div class="col-md-3">
                        <!-- vehiculos -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-scale" aria-hidden="true"></span> Vehículos</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>2]) }}" class="list-group-item">Autos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>3]) }}" class="list-group-item">Motocicletas</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>26]) }}" class="list-group-item">Bicicletas</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>27]) }}" class="list-group-item">Llantas y Rines</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>19]) }}" class="list-group-item">Autopartes y Refacciones</a>
                            </div>
                        </div>
                        <!-- vehiculos -->
                        <!-- inmuebles -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Inmuebles</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>5]) }}" class="list-group-item">Casas</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>28]) }}" class="list-group-item">Departamentos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>6]) }}" class="list-group-item">Terrenos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>29]) }}" class="list-group-item">Oficinas y Locales</a>
                            </div>
                        </div>
                        <!-- inmuebles -->
                    </div>
                    <div class="col-md-3">
                        <!-- gadgets -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> Gadgets</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>1]) }}" class="list-group-item">Smarthpone</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>9]) }}" class="list-group-item">Tablets</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>4]) }}" class="list-group-item">Laptops y PC</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>10]) }}" class="list-group-item">Fotografía</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>22]) }}" class="list-group-item">Alacenamiento</a>
                            </div>
                        </div>
                        <!-- gadgets -->
                        <!-- personal -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Estilos</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>17]) }}" class="list-group-item">Ropa y Zapatos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>18]) }}" class="list-group-item">Belleza</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>11]) }}" class="list-group-item">Personal</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>14]) }}" class="list-group-item">Mascotas</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>30]) }}" class="list-group-item">Variedad</a>
                            </div>
                        </div>
                        <!-- personal -->
                    </div>
                    <div class="col-md-3">
                        <!-- hobby -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> Hobby</h3>
                            </div>
                            <div class="list-group">
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>23]) }}" class="list-group-item">Deportes</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>24]) }}" class="list-group-item">Juguetes</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>25]) }}" class="list-group-item">Instrumentos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>8]) }}" class="list-group-item">Videojuegos</a>
                                <a href="{{ route('searchAdvanced', ['sort'=>'date','category_id'=>20]) }}" class="list-group-item">Audio</a>
                            </div>
                        </div>
                        <!-- hobby -->
                    </div>
                </div>
                @include('partials.ads')
            </div>
            <div class="col-md-1">
            </div>
        </div><!-- row -->
    </div> <!-- /container -->

@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/autocomplete.js') }}"></script>
@endsection