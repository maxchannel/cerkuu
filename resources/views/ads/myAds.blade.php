@extends('layouts.master')

@section('title', 'Mis Anuncios')
@section('meta-description', 'Mis Anuncios')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Mis Anuncios</h1>

                @if (Session::has('message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Publicado</th>
                            <th></th>
                            <th>Titulo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ads as $ad)
                        <tr data-id="{{ $ad->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $ad->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td><a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}"><img src="{{ (count($ad->images)) ? asset('images/ads/'.$ad->images->first()->route) : asset('/images/vendo.png') }}" class="ad-image-min" alt="{{$ad->title}}"></a></td>
                            <td><a href="{{ route('anuncio', [$ad->category->name, $ad->id, $ad->slug->content]) }}">{{ ucfirst($ad->title) }}</a></td>
                            <td>
                                <a href="{{ route('editAd', [$ad->id]) }}" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Editar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! str_replace('/?', '?', $ads->render()) !!}

            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection