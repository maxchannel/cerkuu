<?php

return [
    'smartphone' => 'Smartphone',
    'autos' => 'Autos',
    'motocicletas' => 'Motocicletas',
    'pc' => 'Laptops y PC',
    'casas' => 'Casas',//Antes inmuebles
    'terrenos' => 'Terrenos',//Antes hogar
    'electronics' => 'Electrónicos',
    'videogames' => 'Videojuegos',
    'tablet' => 'Tablets',
    'photography' => 'Fotografía',
    'personal' => 'Personal',
    'electrodomesticos' => 'Electrodomésticos',
    'muebles' => 'Muebles',
    'mascotas' => 'Mascotas',
    'servicios' => 'Servicios',
    'accessory' => 'Accesorios',
    'clothes' => 'Ropa y Zapatos',
    'belleza' => 'Belleza',
    'autopartes' => 'Autopartes',//Antes other
    'audio' => 'Audio',
    'tv' => 'Pantallas',
    'storage' => 'Alacenamiento',
    'sports' => 'Deportes',
    'juguetes' => 'Juguetes',
    'instrumentos' => 'Instrumentos',
    //26 + 5 nuevas
    'bicicletas' => 'Bicicletas',
    'llantas' => 'Llantas',
    'departamentos' => 'Departamentos',
    'oficinas' => 'Oficinas',
    'other' => 'Variety',
];